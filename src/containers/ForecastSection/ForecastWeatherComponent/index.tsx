import * as React from 'react';

interface IWeatherProps{
    className: string;
}

export const WeatherComponent = (props: IWeatherProps) => { 

    return (
        <a className={props.className} href="https://forecast7.com/en/40d71n74d01/new-york/" data-label_1="NEW YORK" data-label_2="WEATHER" data-theme="original" >Text</a>               

    )
}