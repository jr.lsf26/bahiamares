import * as React from "react";
import { useState, useEffect } from "react";
import FormatDate from "../../utils/formatDate";
import FormatHours from "../../utils/formatHours";
import { Modal, Spin, Alert, Calendar, Icon } from "antd";
// import { Document} from 'react-pdf/dist/entry.webpack';

import s from "./style.module.scss";
import ForecastType from "../../components/Type";
// import ServiceList from "../../components/ServicesList";
import IconButton from "../../components/IconButton";

import axios from "axios";
import * as moment from "moment";
import { sunsetRegion } from "../../utils/regions";
import { Document, Page } from "react-pdf";
import { latLongRegion } from "../../utils/latLongValues";
import { ShareList } from "../../components/ShareList";
import { ForecastNavigationBanner } from "../../components/ForecastNavigationBanner";
import { Footer } from '../../components/Footer';
import { ServiceModal } from '../../components/ServiceModal';
import { servicesMocked } from '../../utils/serviceMocks';
import ServiceType from '../../components/ServiceType';
import { MobileForecastBar } from '../../components/ResponsiveForecastBar';
import { HowToReadModal } from '../../components/HowToReadModal';
import { AdditionalInfoModal } from '../../components/AdditionalInfoModal';
import { RoundButton } from '../../components/RoundButton';
// import { Document } from 'react-pdf';

interface IProps {
  region: string;
  forecast: string;
}

const ForecastSection = (props: IProps) => {
  const todayDate = new Date();
  const yearBegin = new Date(todayDate.getFullYear(), 0, 1);
  const today = FormatDate(todayDate);
  const lastFriday = new Date();
  const sevenDaysLimit = moment(todayDate)
    .add(7, "days")
    .toDate();
  const fiveDaysLimit = moment(todayDate)
    .add(5, "days")
    .toDate();
  const fourDaysLimit = moment(todayDate)
    .add(4, "days")
    .toDate();
  const grafTideLimit = new Date(2024, 13, 31);
  const btsCurrentGrafLimit = new Date(2021, 13, 31);
  lastFriday.setDate(new Date().getDate() + (6 - new Date().getDay() - 1) - 7);
  let currentDate = todayDate;
  let hours;
  const [forecastType, setForecastType] = useState(props.forecast);
  const [displayType, setDisplayType] = useState("MAPA");
  (forecastType === "ALERTA" ||
    forecastType === "VENTOS" ||
    (forecastType === "CORRENTES" && props.region === "BTS")) &&
  displayType === "MAPA"
    ? (hours = "06")
    : (hours = "00");
  const minutes = "00";
  const [date, setDate] = useState(today);
  const [selectedDate, setSelectedDate] = useState(todayDate);
  const [limitDate, setLimitdate] = useState(new Date());
  const [hour, setHour] = useState(hours);
  const [minute, setMinute] = useState(minutes);
  const [calendar, setCalendar] = useState(false);
  const [screenLoading, setScreenLoading] = useState(false);
  const [apiImg, setApiImg] = useState();
  const [modalImg, setModalImg] = useState();
  const [modalInfoImg, setModalInfoImg] = useState();
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [serviceDisplay, setServiceDisplay] = useState(false);
  const [qualityWeek, setQualityWeek] = useState(33);
  const [year, setYear] = useState(todayDate.getFullYear().toString());

  const [selectedService, setSelectedService] = useState('');
  const [serviceNumber, setServiceNumber] = useState(0);
  const [showServiceModal, setShowServiceModal] = useState(false);

  const[regionCheck, setRegionCheck] = useState(props.region);

  const[curWidth, setCurWidth] = React.useState();

  const[pdfPage, setPdfPage] = React.useState(1);
  const [modalLoading, setModalLoading] = React.useState(false);

  const shareModal = Modal.info;

  const changeDate = (newDate: moment.Moment) => {
    hours = FormatHours(newDate.toDate());
    currentDate = newDate.toDate();
    setSelectedDate(currentDate);
    const formatted = FormatDate(newDate.toDate());
    setDate(formatted);
  };

  const weeksBetween = (initialDate: Date, finalDate: Date) => {
    const weeks = Math.round(
      (finalDate.valueOf() - initialDate.valueOf()) / (7 * 24 * 60 * 60 * 1000)
    );
    setQualityWeek(weeks);
  };

  const useWeatherScript = () => {
    let js;
    const fjs = document.getElementsByTagName("script")[0];
    if (!document.getElementById("weatherwidget-io-js")) {
      js = document.createElement("script");
      js.id = "weatherwidget-io-js";
      js.src = require("./weatherScript.js");
      js.async = true;
      fjs.parentNode!.insertBefore(js, fjs);
    }
  };

  const checkDaysLimit = () => {
    if (
      (forecastType === "ALERTA" || forecastType === "ONDAS") &&
      displayType === "MAPA"
    ) {
      return sevenDaysLimit;
    }
    if (forecastType === "VENTO" && displayType === "MAPA") {
      return fourDaysLimit;
    }
    if (forecastType === "MARE" && displayType === "GRAF") {
      return grafTideLimit;
    }
    if (
      forecastType === "CORRENTES" &&
      displayType === "GRAF" &&
      props.region === "BTS"
    ) {
      return btsCurrentGrafLimit;
    }
    return fiveDaysLimit;
  };

  const initialHour = () => {
    if (
      displayType === "MAPA" &&
      forecastType === "CORRENTES" &&
      props.region === "BTS"
    ) {
      setHour("05");
      return;
    }

    if (
      forecastType === "ALERTA" ||
      forecastType === "VENTO" ||
      (forecastType === "CORRENTES" && displayType === "LITORAL") ||
      displayType === "MAPA"
    ) {
      setHour("06");
      return;
    }
    setHour("00");
    return;
  };

  useEffect(() => {
    if(props.region !== regionCheck){
      setRegionCheck(props.region);
      fetchImage();
    }
  })

  useEffect(() => {
    if (displayType === "CIENCIA") {
      setPdfPage(1);
    }
  }, [displayType]);

  useEffect(() => {
    setDisplayType("MAPA");
    if (forecastType === "SOS") {
      setDisplayType("AVISOS");
    }
    fetchModalImg();
    fetchModalInfoImg();
    setErrorMessage(null);
  }, [forecastType]);

  useEffect(() => {
    setCurWidth(window.innerWidth)
    initialHour();
    fetchImage();
    setLimitdate(checkDaysLimit());
    weeksBetween(yearBegin, new Date());
    useWeatherScript();
    
  }, []);

  useEffect(() => {
    initialHour();
    setDate(today);
    setYear(todayDate.getFullYear().toString());
    setLimitdate(checkDaysLimit());
    fetchModalInfoImg();

    if (forecastType === "TEMPO" && displayType === "GRAF") {
      useWeatherScript();
    }
  }, [forecastType, displayType]);

  useEffect(() => {
    if(displayType === "CIENCIA" || displayType === "SOS"){
      fetchImage();
    }
    else{
      fetchModalImg();
      fetchModalInfoImg();
    }
  },[pdfPage])

  useEffect(() => {
    setMinute(minute);
    setServiceDisplay(false);
    fetchImage();
    setLimitdate(checkDaysLimit());
  }, [forecastType, date, hour, year, minute, displayType]);

  useEffect(() => {
    if (serviceDisplay === true) {
      setApiImg(null);
    }
  }, [serviceDisplay]);

  const showCalendar = () => {
    setCalendar(!calendar);
  };

  const disableDate = (current: moment.Moment) => {
    return current > moment(limitDate) || current < moment().startOf("day");
  };

  const hasMapa = (type: string) => {
    if (type === "QUALIDADE" || type === "MARE") {
      return false;
    } else {
      return true;
    }
  };

  const hasGraph = (type: string) => {
    if (
      type === "QUALIDADE" ||
      type === "TSM" ||
      (type === "CORRENTES" && props.region !== "BTS")
    ) {
      return false;
    } else {
      return true;
    }
  };

  const hasHours = (type: string) => {
    if (
      displayType === "SATELITE" ||
      type === "ALERTA" ||
      (type === "CORRENTES" && displayType !== "GRAF") ||
      type === "VENTO"
    ) {
      return true;
    } else {
      return false;
    }
  };

  const previousHour = () => {
    let auxHour = +hour;
    if (
      displayType === "MAPA" &&
      forecastType === "CORRENTES" &&
      props.region === "BTS"
    ) {
      auxHour = auxHour - 1;
    } else {
      auxHour = auxHour - 3;
    }
    if (auxHour < 10) {
      hours = "0" + auxHour.toString();
      setHour(hours);
    } else {
      hours = auxHour.toString();
      setHour(hours);
    }
  };

  const nextHour = () => {
    let auxHour = +hour;
    if (
      displayType === "MAPA" &&
      forecastType === "CORRENTES" &&
      props.region === "BTS"
    ) {
      auxHour = auxHour + 1;
    } else {
      auxHour = auxHour + 3;
    }
    if (auxHour < 10) {
      hours = "0" + auxHour.toString();
      setHour(hours);
    } else {
      hours = auxHour.toString();
      setHour(hours);
    }
  };

  const disableNextButton = () => {
    if (
      displayType === "LUNAR" &&
      forecastType === "MARE" &&
      year ===
        moment(todayDate)
          .add(3, "year")
          .toDate()
          .getFullYear()
          .toString()
    ) {
      return true;
    }
    if (
      displayType === "MAPA" &&
      forecastType === "CORRENTES" &&
      +hour === 18
    ) {
      return true;
    }
    if (
      forecastType === "CORRENTES" &&
      displayType === "LITORAL" &&
      +hour === 15
    ) {
      return true;
    }
    if (
      (!(displayType === "LUNAR" && forecastType === "MARE") && +hour === 18) ||
      selectedDate >= limitDate
    ) {
      return true;
    }
    return false;
  };

  const disablePreviousButton = () => {
    if (
      displayType === "LUNAR" &&
      forecastType === "MARE" &&
      year === todayDate.getFullYear().toString()
    ) {
      return true;
    }
    if (displayType === "MAPA" && forecastType === "CORRENTES" && +hour === 5) {
      return true;
    }
    if (!(displayType === "LUNAR" && forecastType === "MARE") && +hour === 6) {
      return true;
    }
    if (
      !(displayType === "LUNAR" && forecastType === "MARE") &&
      +hour <= 0 &&
      selectedDate <= todayDate
    ) {
      return true;
    }
    return false;
  };

  const nextDay = () => {
    if (displayType === "LUNAR") {
      setYear((+year + 1).toString());
      return;
    }
    const nextDate = moment(selectedDate)
      .add(1, "day")
      .toDate();
    setSelectedDate(nextDate);
    setDate(FormatDate(nextDate));
  };

  const previousDay = () => {
    if (displayType === "LUNAR") {
      setYear((+year - 1).toString());
      return;
    }
    const previousDate = moment(selectedDate)
      .add(-1, "day")
      .toDate();
    setSelectedDate(previousDate);
    setDate(FormatDate(previousDate));
  };

  const checkLitoralRegion = () => {
    if (
      ((forecastType === "ALERTA" || forecastType === "VENTO") &&
        displayType === "MAPA") ||
      forecastType === "MARE"
    ) {
      return true;
    }
    return false;
  };


  const fetchModalImg = async () => {
    setErrorMessage(null);
    setModalLoading(true);
    axios
      .get("https://bahiamares.com.br:3000/assets/HELP", {
        responseType: "arraybuffer",
        params: {
          forecastType: forecastType,
          region: props.region,
          displayType: displayType,
          page: pdfPage
        }
      })
      .then(response => {
        const modalImg = new Buffer(response.data, "binary").toString("base64");
        setModalImg(modalImg);
      })
      .catch(() => {
        setModalImg(null);
      })
      .finally(() => {
        setModalLoading(false);
      });
  };

  const changeSelectedService = (service: string) => { 
    setSelectedService(service)
    setShowServiceModal(true)
  }

  const fetchModalInfoImg = async () => {
    setErrorMessage(null);
    setModalLoading(true);
    axios
      .get("https://bahiamares.com.br:3000/assets/INFO", {
        responseType: "arraybuffer",
        params: {
          forecastType: forecastType,
          region: props.region,
          displayType: displayType, 
          page: pdfPage
        }
      })
      .then(response => {
        const modalImg = new Buffer(response.data, "binary").toString("base64");
        setModalInfoImg(modalImg);
      })
      .catch(() => {
        setModalInfoImg(null);
      })
      .finally(() => {
        setModalLoading(false);
      });
  };

  const fetchImage = async () => {
    setErrorMessage(null);
    checkLitoralRegion();
    setScreenLoading(true);
    axios
      .get("https://bahiamares.com.br:3000/assets/" + forecastType, {
        responseType: "arraybuffer",
        params: {
          displayType: displayType,
          forecastType: forecastType,
          region: props.region,
          date: date,
          hour: hour,
          minute: minute,
          weekNumber: qualityWeek,
          year: year,
          page: pdfPage,
          width: curWidth
        }
      })
      .then(response => {
        const displayImg = new Buffer(response.data, "binary").toString(
          "base64"
        );
        setApiImg(displayImg);
      })
      .catch(() => {
        setApiImg(null);
      })
      .finally(() => {
        setScreenLoading(false);
      });
  };

  const mapaTooltipText = () => {
    if (displayType === "MAPA" && forecastType === "TEMPO") {
      return "Carta Sinótica";
    } else {
      return "Mapa";
    }
  };

  const showShareModal = () => {
    shareModal({
      title: "Compartilhar!",
      maskClosable: true,
      width: "20%",
      content: (
        <ShareList
          url={`bahiamares.com.br/previsoes/${props.region}`}
        ></ShareList>
      )
    });
  };

  const [howToReadModalVisible, setHowToReadModalVisible] = React.useState(false);
  const [additionInfoModalVisible, setAdditionInfoModalVisible] = React.useState(false);



  return (
    <Spin spinning={screenLoading} tip="Carregando previsão...">
      {selectedService && ( 
        <ServiceModal
      address={servicesMocked[selectedService][serviceNumber].address}
      contacts={servicesMocked[selectedService][serviceNumber].contacts}
      logos={servicesMocked[selectedService][serviceNumber].logos}
      onCancel={() => setShowServiceModal(false)}
      serviceType={selectedService}
      serviceNumber={serviceNumber}
      serviceName={servicesMocked[selectedService][serviceNumber].name}
      setServiceNumber={setServiceNumber}
      visible={showServiceModal}
      maxPage={servicesMocked[selectedService].length}
      />
      )}

      <HowToReadModal
      loading={modalLoading}
      page={pdfPage}
      setPdfPage={setPdfPage}
      currentImg={modalImg}
      displayType={displayType}
      forecastType={forecastType}
      onClose={
        () => {
          setPdfPage(1);
          setHowToReadModalVisible(false)
        }
      }
      visible={howToReadModalVisible}/>
       <AdditionalInfoModal
       loading={modalLoading}
      page={pdfPage}
      setPdfPage={setPdfPage}
      currentImg={modalInfoImg}
      forecastType={forecastType}
      onClose={
        () => {
          setPdfPage(1);
          setAdditionInfoModalVisible(false)
        }
      }
      visible={additionInfoModalVisible}/>

      
      <section className={s.section}>
        <ForecastNavigationBanner
          forecast={forecastType}
        ></ForecastNavigationBanner>
        <div className={s.mainContainer}>
          <section id="predict" className={s.types}>
            <p className={s.topService}>Preveja</p>
            <div
              className={s.serviceBtn}
              onClick={() => {
                setForecastType("ALERTA");
                setServiceDisplay(false);
              }}
            >
              <ForecastType
                title="Alerta Oceânico"
                imgSrc="alert"
              ></ForecastType>
              <h4>Alerta Oceânico</h4>
            </div>
            <div
              className={s.serviceBtn}
              onClick={() => {
                setForecastType("ONDAS");
                setServiceDisplay(false);
              }}
            >
              <ForecastType title="Ondas" imgSrc="wave"></ForecastType>
              <h4>Ondas</h4>
            </div>
            <div
              className={forecastType === "VENTO" ? s.focus : s.serviceBtn}
              onClick={() => {
                setForecastType("VENTO");
              }}
            >
              <ForecastType title="Vento" imgSrc="wind"></ForecastType>
              <h4>Vento</h4>
            </div>
            <div
              className={forecastType === "MARE" ? s.focus : s.serviceBtn}
              onClick={() => {
                setForecastType("MARE");
              }}
            >
              <ForecastType title="Maré" imgSrc="tide"></ForecastType>
              <h4>Maré</h4>
            </div>
            <div
              className={forecastType === "CORRENTES" ? s.focus : s.serviceBtn}
              onClick={() => {
                setForecastType("CORRENTES");
              }}
            >
              <ForecastType
                title="Correntes"
                imgSrc="currents-icon"
              ></ForecastType>
              <h4>Correntes</h4>
            </div>
            <div
              className={forecastType === "TEMPO" ? s.focus : s.serviceBtn}
              onClick={() => {
                setForecastType("TEMPO");
              }}
            >
              <ForecastType title="Tempo" imgSrc="weather"></ForecastType>
              <h4>Tempo</h4>
            </div>
            <div
              className={forecastType === "QUALIDADE" ? s.focus : s.serviceBtn}
              onClick={() => {
                setForecastType("QUALIDADE");
              }}
            >
              <ForecastType
                title="Qualidade da Água"
                imgSrc="water-quality"
              ></ForecastType>
              <h4>Qualidade da Água</h4>
            </div>
            <div
              className={forecastType === "TSM" ? s.focus : s.serviceBtn}
              onClick={() => {
                setForecastType("TSM");
              }}
            >
              <ForecastType
                title="Temperatura da Superfície do Mar"
                imgSrc="water-temperature"
              ></ForecastType>
              <h4>Temperatura da Superfície do Mar</h4>
            </div>
            <div
              className={forecastType === "SOS" ? s.focus : s.serviceBtn}
              onClick={() => {
                setForecastType("SOS");
              }}
            >
              <ForecastType title="SOS" imgSrc="boia"></ForecastType>
              <h4>SOS</h4>
            </div>
          </section>
          <section id="map" className={s.centerBox}>
            {errorMessage !== null && (
              <Alert
                message={errorMessage}
                type="error"
                showIcon
                closable
              ></Alert>
            )}
            <div  id="mapTopMenu" className={s.boxWithLineMenu}>
              {forecastType === "MARE" && (
                <div onClick={() => setDisplayType("LUNAR")} className={displayType === "LUNAR" ? s.topMenuBtnSelected : s.topMenuBtn}>
                <IconButton
                  isSelected={displayType === "LUNAR"}
                  onClick={() => setDisplayType("LUNAR")}
                  tooltip="Calendário Lunar"
                  img={"moon"}
                />
                <h4 className={s.textBtn}>Calendário Lunar</h4>
                </div>
              )}
              {forecastType === "TEMPO" && (
                 <div onClick={() => setDisplayType("SUNSET")} className={displayType === "SUNSET" ? s.topMenuBtnSelected : s.topMenuBtn}>
                <IconButton
                  isSelected={displayType === "SUNSET"}
                  onClick={() => setDisplayType("SUNSET")}
                  tooltip="Horário do Pôr-do-Sol"
                  img={"sunset"}
                />
                   <h4 className={s.textBtn}>Horário do Pôr-do-Sol</h4>
                </div>
              )}
              {forecastType === "SOS" && (
                <>
                <div onClick={() => setDisplayType("AVISOS")} className={displayType === "AVISOS" ? s.topMenuBtnSelected : s.topMenuBtn}>
                  <IconButton
                    isSelected={displayType === "AVISOS"}
                    onClick={() => setDisplayType("AVISOS")}
                    img={"warning"}
                  />
                  <h4 className={s.textBtn}>Avisos</h4>
                </div>
                <div onClick={() => setDisplayType("SOCORROS")} className={displayType === "SOCORROS" ? s.topMenuBtnSelected : s.topMenuBtn}>
                  <IconButton
                    isSelected={displayType === "SOCORROS"}
                    onClick={() => setDisplayType("SOCORROS")}
                    img={"firstaid"}
                 />
                  <h4 className={s.textBtn}>Emergências!</h4>
                </div>
                <div onClick={() => setDisplayType("RETORNO")} className={displayType === "RETORNO" ? s.topMenuBtnSelected : s.topMenuBtn}>
                  <IconButton
                    isSelected={displayType === "RETORNO"}
                    onClick={() => setDisplayType("RETORNO")}
                    tooltip="Correntes de Retorno"
                    img={"drowning"}
                  />
                  <h4 className={s.textBtn}>Correntes de Retorno</h4>
                </div>
                <div onClick={() => setDisplayType("ANIMAIS")} className={displayType === "ANIMAIS" ? s.topMenuBtnSelected : s.topMenuBtn}>
                  <IconButton
                    isSelected={displayType === "ANIMAIS"}
                    onClick={() => setDisplayType("ANIMAIS")}
                    tooltip="Animais Peçonhentos!"
                    img={"jellyfish"}
                  />
                  <h4 className={s.textBtn}>Animais Peçonhentos!</h4>
                </div>
                </>
              )}

              {forecastType !== "SOS" && (
                <>
 {forecastType === "CORRENTES" && props.region === "BTS" && (
                <div onClick={() => setDisplayType("LITORAL")} className={displayType === "LITORAL" ? s.topMenuBtnSelected : s.topMenuBtn}>
                <IconButton
                  onClick={() => setDisplayType("LITORAL")}
                  isSelected={displayType === "LITORAL"}
                  tooltip="Correntes no Litoral"
                  img={"currents-icon"}
                ></IconButton>
                <h4 className={s.textBtn}>Correntes no Litoral</h4>
                </div>
              )}


                {forecastType === "ONDAS" && (
                  <div onClick={() => setDisplayType("GIF")} className={displayType === "GIF" ? s.topMenuBtnSelected : s.topMenuBtn} >
                <IconButton
                  isSelected={displayType === "GIF"}
                  tooltip="Executar animação Gif"
                  onClick={() => setDisplayType("GIF")}
                  img={"play"}
                />
                <h4 className={s.textBtn}>GIF</h4>
                </div>
                ) }
                
                  <div onClick={() => setDisplayType("GRAF")} className={displayType === "GRAF" ? s.topMenuBtnSelected : s.topMenuBtn}>
                    <IconButton
                      isSelected={displayType === "GRAF"}
                      onClick={() => setDisplayType("GRAF")}
                      tooltip={
                        !hasGraph(forecastType)
                          ? "Gráfico indisponível"
                          : "Gráfico"
                      }
                      img="graph-icon"
                      disable={!hasGraph(forecastType) ? true : false}
                    />
                    <h4 className={s.textBtn}>Gráfico</h4>
                  </div>
                  {forecastType === "TEMPO" && (
                    <div onClick={() => setDisplayType("SATELITE")} className={displayType === "SATELITE" ? s.topMenuBtnSelected : s.topMenuBtn}>
                      <IconButton
                        isSelected={displayType === "SATELITE"}
                        onClick={() => setDisplayType("SATELITE")}
                        tooltip="Satelite"
                        img={"satelite"}
                      ></IconButton>
                      <h4 className={s.textBtn}>Satelite</h4>
                    </div>
                  )}
                  <div  onClick={() => setDisplayType("MAPA")} className={displayType === "MAPA" ? s.topMenuBtnSelected : s.topMenuBtn}>
                    <IconButton
                      isSelected={displayType === "MAPA"}
                      onClick={() => setDisplayType("MAPA")}
                      tooltip={
                        !hasMapa(forecastType)
                          ? "Mapa indisponível"
                          : mapaTooltipText()
                      }
                      img="map-icon"
                      disable={!hasMapa(forecastType) ? true : false}
                    ></IconButton>
                    <h4 className={s.textBtn}>Mapa</h4>
                  </div>
                </>
              )}
              {forecastType !== "SOS" && forecastType !== "QUALIDADE" && (
                <div onClick={() => setDisplayType("CIENCIA")} className={displayType === "CIENCIA" ? s.topMenuBtnSelected : s.topMenuBtn}>
                  <IconButton
                    isSelected={displayType === "CIENCIA"}
                    tooltip={"Ciência!"}
                    onClick={() => setDisplayType("CIENCIA")}
                    img="science"
                  ></IconButton>
                  <h4 className={s.textBtn}>Ciência</h4>
                </div>
              )}
              <div onClick={() => setAdditionInfoModalVisible(true)} className={s.topMenuBtn}>
                <IconButton
                  tooltip={"Informações adicionais"}
                  img="info"
                ></IconButton>
                <h4 className={s.textBtn}>Informações adicionais</h4>
              </div>
              <div onClick={() => setHowToReadModalVisible(true)} className={s.topMenuBtn}>
                <IconButton
                  tooltip={"Como interpretar"}
                  img="questions"
                ></IconButton>
                <h4 className={s.textBtn}>Como interpretar</h4>
              </div>
              <div className={s.topMenuBtn}>
                <a
                  className={apiImg ? s.disable : ""}
                  download="DOWNLOAD.PNG"
                  href={`data:image/png;base64, ` + apiImg}
                >
                  <IconButton
                    tooltip={"Download"}
                    disable={apiImg ? false : true}
                    img="download-icon"
                  ></IconButton>
                  <h4 className={s.textBtn}>Download</h4>
                </a>
              </div>
              <div onClick={() => showShareModal()} className={s.topMenuBtn}>
                <IconButton
                  onClick={() => showShareModal()}
                  tooltip={"Compartilhar"}
                  img="share"
                ></IconButton>
                <h4 className={s.textBtn}>Compartilhar</h4>
              </div>
            </div>
            <div className={s.imgBox}>
            {apiImg !== null &&
              serviceDisplay === false &&
              forecastType !== "QUALIDADE" &&
              !(forecastType === "TEMPO" && displayType === "SUNSET") &&
              !(forecastType === "SOS" || displayType === "CIENCIA") &&
              !(forecastType === "TEMPO" && displayType === "GRAF") &&
              forecastType !== "SOS" && (
                <img
                  className={displayType === "GRAF" ? s.forecastImageWithMargin :  s.forecastImage}
                  src={`data:image/png;base64, ` + apiImg}
                ></img>
              )}

            {apiImg === null &&
              !(forecastType === "TEMPO" && displayType === "SUNSET") &&
              !(forecastType === "TEMPO" && displayType === "GRAF") &&
              displayType !== "CIENCIA" &&
              serviceDisplay === false && (
                <img
                  className={s.forecastImage}
                  src={require("../../assets/images/not-found.png")}
                ></img>
              )}
            {forecastType === "QUALIDADE" &&
              apiImg !== null &&
              serviceDisplay === false && (
                curWidth < 769 ? 
                <>
                <Document
                className={s.qualityPdf}
                file={`data:application/pdf;base64,${apiImg}`}
                renderMode="canvas"
              >
                <Page pageNumber={pdfPage} />
              </Document>
              <div className={s.buttonsDivScience}>
                  {pdfPage > 1 && <RoundButton onClick={() => setPdfPage(pdfPage-1)} icon={<Icon className={s.btnIcon}  type="left"/>}/>}
                  {pdfPage < 2 && <RoundButton onClick={() => setPdfPage(pdfPage+1)} icon={<Icon className={s.btnIcon} type="right"/>}/>}
                  </div>
              </>
              :
              <iframe
                  className={s.forecastPdf}
                  src={`data:application/pdf;base64, ${apiImg}`}
                  height="100%" width="100%"
                />
              )
            }
            {(forecastType === "SOS" || displayType === "CIENCIA") &&
              apiImg !== null &&
              serviceDisplay === false && (
                curWidth < 769 ? 
                <>
                {/* <Document
                  className="pdf-view-port"
                  file={`data:application/pdf;base64,${apiImg}`}
                  renderMode="canvas"
                >
                
                  <Page pageNumber={pdfPage} />
                </Document> */}
                <img
                className={s.forecastPdf}
                 src={apiImg ? `data:application/png;base64, ` + apiImg : require("../../assets/images/not-found.png")}
                ></img>
                  <div className={s.buttonsDivScience}>
                  {pdfPage > 1 && <RoundButton onClick={() => setPdfPage(pdfPage-1)} icon={<Icon className={s.btnIcon}  type="left"/>}/>}
                  {pdfPage < 3 && <RoundButton onClick={() => setPdfPage(pdfPage+1)} icon={<Icon className={s.btnIcon} type="right"/>}/>}
                  </div>
                  </>
                  : <iframe
                  className={s.forecastPdf}
                  src={`data:application/pdf;base64, ${apiImg}`}
                  height="100%" width="100%"
                />
              )}

            {serviceDisplay === true &&
              !(forecastType === "TEMPO" && displayType === " SUNSET") && (
                <img
                  className={s.serviceImage}
                  src={require("../../assets/SERVICO_BTS_TESTE_01.png")}
                ></img>
              )}

            {forecastType === "TEMPO" &&
              displayType === "SUNSET" &&
              serviceDisplay === false &&
              apiImg === null && (
                <iframe
                  src={`https://sunsetsunrisetime.com/widget.php?view=true&newdb=true&idcity=${
                    sunsetRegion[props.region]
                  }&time=-3&days=7&text=000000`}
                  scrolling="no"
                  width="100%"
                  height="280"
                ></iframe>
              )}
            {forecastType === "TEMPO" &&
              displayType === "GRAF" &&
              serviceDisplay === false &&
              apiImg === null && (
                <>
                  <iframe
                  className={s.darkSky}
                    id="forecast_embed"
                    height="245"
                    width="500px "
                    src={`https://forecast.io/embed/#${
                      latLongRegion[props.region]
                    }&units=ca&lang=pt`}
                  />
                </>
              )}
</div>
            <div className={s.arrowsBox}>
              <IconButton
                disable={disablePreviousButton()}
                tooltip={"Anterior"}
                onClick={() =>
                  hasHours(forecastType) ? previousHour() : previousDay()
                }
                img="previous"
              ></IconButton>

              <div className={s.calendars}>
                <IconButton
                  tooltip={"Calendário"}
                  disable={forecastType === "TEMPO"}
                  img="calendar"
                  onClick={() => showCalendar()}
                  hasLabel={true}
                ></IconButton>
                {calendar ? (
                  <Calendar
                    locale={{
                      today: "Hoje",
                      month: "Mês",
                      year: "Ano",
                      dateSelect: "Selecione a Data",
                      dateFormat: "DD/MM/YYYY"
                    }}
                    monthFullCellRender={month => moment(month).locale("pt-BR")}
                    monthCellRender={month => moment(month).locale("pt-BR")}
                    disabledDate={current => disableDate(current)}
                    onChange={newDate => changeDate(newDate!)}
                    fullscreen={false}
                  ></Calendar>
                ) : null}
              </div>

              <IconButton
                disable={disableNextButton()}
                tooltip={"Próximo"}
                onClick={() =>
                  hasHours(forecastType) ? nextHour() : nextDay()
                }
                img="next"
              ></IconButton>
            </div>
          </section>
        </div>
        <section id="services" className={s.services}>
          <h2>Serviços Naúticos na Região</h2>
          <div className={s.serviceContainer}>
          <ServiceType handleClick={changeSelectedService} serviceName={"Surf"} imgSrc="surf"></ServiceType>
          <ServiceType disabled handleClick={changeSelectedService}  serviceName={"Kite-surf"}  imgSrc="kitesurf"></ServiceType>
          <ServiceType disabled handleClick={changeSelectedService}  serviceName={"Mergulho"}  imgSrc="diving"></ServiceType>
          <ServiceType disabled handleClick={changeSelectedService} serviceName={"Canoagem"}  imgSrc="slalom"></ServiceType>
          <ServiceType disabled handleClick={changeSelectedService} serviceName={"Fotografia"}  imgSrc="photography"></ServiceType>
          <ServiceType disabled handleClick={changeSelectedService} serviceName={"Drone"}  imgSrc="drone"></ServiceType>
          <ServiceType disabled handleClick={changeSelectedService} serviceName={"Passeio"}  imgSrc="passeio-iate"></ServiceType>
          <ServiceType disabled handleClick={changeSelectedService} serviceName={"Naútica"}  imgSrc="nautica"></ServiceType>
          <ServiceType disabled handleClick={changeSelectedService} serviceName={"SUP"}  imgSrc="standup-paddle"></ServiceType>
          </div>
        </section>
      </section>
      {curWidth < 769 && (
        <MobileForecastBar forecast={forecastType} setForecastType={setForecastType} setServiceDisplay={setServiceDisplay}/>
      )}
      <Footer setFeedbackModal={() => console.log("hmm")}></Footer>
    </Spin>
  );
};

export default ForecastSection;
