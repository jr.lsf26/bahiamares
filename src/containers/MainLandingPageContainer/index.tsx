import * as React from "react";

import s from "./style.module.scss";
import { RouteComponentProps , navigate } from '@reach/router';

import { Button, Icon, notification } from 'antd';
import axios from "axios";
import AliceCarousel from 'react-alice-carousel'
import 'react-alice-carousel/lib/alice-carousel.css'
import { ForecastInfoBar } from '../../components/ForecastInfoBar';
import { IconWithText } from '../../components/IconWithText';
import { CreatorDisplay } from '../../components/CreatorDisplay';
import { UserComments } from '../../components/UserComments';
import { Footer } from '../../components/Footer';
import { Header } from '../../components/Header';
import { ChangeRegionForm } from '../../components/ChangeRegionForm';
import { IInfoContent, forecastInfoContent } from '../../utils/forecastInfoBarContent';
import { ForecastInfoModal } from '../../components/ForecastInfoModal';
import { SponsorForm } from '../../components/SponsorForm';
import { PartnerForm } from '../../components/PartnerForm';
import { FeedbackForm } from '../../components/FeedbackForm';
import { forecastsVariables, regionsVariables } from '../../components/Navbar';
import { ScrollTopButton } from '../../components/ScrollTopButton';

const MainContainer = (props: RouteComponentProps) => {

  const [changeRegionVisible, setChangeRegionVisible] = React.useState(false);
  const [sponsorModalVisible, setSponsorModalVisible] = React.useState(false);
  const [partnerModalVisible, setPartnerModalVisible] = React.useState(false);
  const [feedbackModalVisible, setFeedbackModalVisible] = React.useState(false);
  const [forecastInfoObject, setForecastInfoObject] = React.useState<IInfoContent>({
    title: '',
    info: [],
    imgSrc: 'Alerta'
  });
  const [forecastInfoModalVisible, setForecastInfoModalVisible] = React.useState(false);
  const [selectedForecast, setSelectedForecast] = React.useState('ALERTA');


  const blogRef = React.createRef<HTMLDivElement>();
  const contactRef = React.createRef<HTMLDivElement>();
  const aboutRef = React.createRef<HTMLDivElement>();


  console.log(props)
  
  const fakeSubmit = (values: any) => { 
    const sacredPath = `/previsoes/${regionsVariables[values.region.replace(/\s/g, "")]}/${selectedForecast}`
    navigate(sacredPath)
 
  }

  const changeForecastModalInfo =(key: string) => { 
      setForecastInfoObject(forecastInfoContent[key]);
      setForecastInfoModalVisible(true)
  }

  const onClickForecast = (forecast: string) => { 
    setSelectedForecast(forecastsVariables[forecast.replace(/\s/g, "").replace(".", "")])
    setChangeRegionVisible(true)
  }

  const sendEmailContact = (textContent: any) => { 
    axios
      .get("https://bahiamares.com.br:3000/email/contact" , {
        params: {
          name: textContent.name,
          email: textContent.email,
          telephone: textContent.phone,
          message: textContent.message,
          type: textContent.type
        }
      }).then(res => {
        console.log(res)
        setFeedbackModalVisible(false);
        setPartnerModalVisible(false);
        setSponsorModalVisible(false);
        notification.open({
          message: 'E-mail enviado com sucesso!',
          description:
            'Seu e-mail foi enviado com sucesso. Agradecemos o contato e iremos retornar em breve!',
        });
      }).catch(error => { 
        console.log(error)
      })
  }

  const scrollToTop = () => { 
    window.scrollTo({top: 0, behavior: "smooth",});
  }
  const scrollToAbout = () => { 
    aboutRef!.current!.scrollIntoView({ behavior: 'smooth', block: 'start' })
  }
  const scrollToBlog = () => { 
    blogRef!.current!.scrollIntoView({ behavior: 'smooth', block: 'start' })
  }

  return (
    <main className={s.mainContainer}>
      <Header openContactModal={() => setFeedbackModalVisible(true)} scrollToBlog={scrollToBlog} scrollToAbout={scrollToAbout} onClickForecast={onClickForecast}></Header>
      <ChangeRegionForm
      closeModal={() => setChangeRegionVisible(false)}
      onSubmit={fakeSubmit}
      visible={changeRegionVisible}
      />
      <ForecastInfoModal
      visible={forecastInfoModalVisible}
      infoContent={forecastInfoObject}
      onCancel={() => setForecastInfoModalVisible(false)}
      />
      <SponsorForm
      closeModal={() => setSponsorModalVisible(false)}
      onSubmit={sendEmailContact}
      visible={sponsorModalVisible}
      />
      <PartnerForm
      closeModal={() => setPartnerModalVisible(false)}
      visible={partnerModalVisible}
      onSubmit={sendEmailContact}
      />

      <FeedbackForm 
      closeModal={() => setFeedbackModalVisible(false)}
      visible={feedbackModalVisible}
      onSubmit={sendEmailContact}
      />
      <div className={s.homeCenter}>
          <div className={s.container}>
                <div className={s.title}>
                    <h1 className={s.titleText}>Você e o mar<br/> conect@dos!</h1>
                    <p className={s.subtitle}>
                    Preveja agora as condições do mar e do
                    <br/>
tempo para sua atividade no litoral da Bahia:
                    </p>
                    <div className={s.buttonBox}>
                    <Button onClick={() => {setSelectedForecast("ALERTA"); setChangeRegionVisible(true)}} className={s.btn}>Selecionar região</Button>
                    </div>
                </div>
          </div>
          <section className={s.forecastsContainer}>
            <h4>Oferecemos para você:</h4>
            <span>Previsões e informações do mar e do tempo</span>
            <div className={s.barsBox}>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("ALERTA")} title="Alerta Oceânico" iconSrc="alerta-oceanico"/>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("ONDAS")}  title="Ondas" iconSrc="ondas"/>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("VENTOS")}  title="Ventos" iconSrc="ventos"/>
            </div>
            <div className={s.barsBox}>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("MARE")}  title="Maré/Calendário Lunar" iconSrc="mare"/>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("CORRENTES")}  title="Correntes no Litoral" iconSrc="correntes-no-litoral"/>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("CORRENTESBTS")}  title="Correntes na BTS" iconSrc="correntes-na-bts"/>
            </div>
            <div className={s.barsBox}>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("TEMPO")}  title="Tempo" iconSrc="tempo"/>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("SATELITE")}  title="Imagem de Satélite" iconSrc="imagem-de-satelite"/>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("SINOTICA")}  title="Carta Sinótica do Tempo" iconSrc="carta-sinotica-do-tempo"/>
            </div>
            <div className={s.barsBox}>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("QUALIDADE")}  title="Qualidade da Água" iconSrc="qualidade-da-agua"/>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("TEMPERATURA")}  title="Temp. da Água" iconSrc="temperatura-agua-2"/>
            <ForecastInfoBar onClick={() => changeForecastModalInfo("SOS")}  title="SOS" iconSrc="sos"/>
            </div>
          </section>

          <section className={s.activitiesContainer}>
            <h4>Ideal para sua atividade</h4>
           <AliceCarousel  autoPlayInterval={5000} duration={1000} fadeOutAnimation autoPlay={true} buttonsDisabled mouseTrackingEnabled>
           
               <img className={s.sliderImg} src={require('../../assets/images/servicesImg/0.jpg')}></img>
               <img className={s.sliderImg}  src={require('../../assets/images/servicesImg/1.jpg')}></img>
               <img className={s.sliderImg}  src={require('../../assets/images/servicesImg/2.jpg')}></img>
               <img className={s.sliderImg}  src={require('../../assets/images/servicesImg/3.jpg')}></img>
               <img className={s.sliderImg}  src={require('../../assets/images/servicesImg/4.jpg')}></img>
               <img className={s.sliderImg}  src={require('../../assets/images/servicesImg/5.jpg')}></img>
               <img className={s.sliderImg}  src={require('../../assets/images/servicesImg/6.jpg')}></img>
               <img className={s.sliderImg}  src={require('../../assets/images/servicesImg/7.jpg')}></img>
           </AliceCarousel>
          </section>

          <section ref={aboutRef} className={s.aboutContainer}>
            <h4>Sobre nós</h4>
            <p>A plataforma bahiamares, idealizada por oceanógrafos, tem como principal missão: facilitar o acesso e a compreensão de previsões e informações meteo-oceanográficas que impactam na sua vida no litoral da Bahia.</p>
            <br/>
           
            <p>Atendemos 2 objetivos propostos pela Organização das Nações Unidas (ONU) para a Década do Desenvolvimento Sustentável (2021-2030):</p>
            <div className={s.okText}>
            <Icon className={s.okIcon} type="check-circle" theme="filled" twoToneColor="#52c41a"></Icon>
             <p>Um oceano previsto, em que a sociedade tem a capacidade de compreender as condições oceânicas atuais e futuras, prevê a sua mudança e o impacto sobre o bem-estar humano.</p>
            </div>
            <div className={s.okText}>
            <Icon className={s.okIcon} type="check-circle" theme="filled" twoToneColor="#52c41a"></Icon>
             <p>Um oceano transparente e acessível, em que os cidadãos tenham acesso a dados, informações oceânicas e tecnologias, e tenham a capacidade de tomar suas decisões.</p>
            </div>

            <p>A bahiamares é uma plataforma inovadora que foi desenvolvida a partir do know-how em Oceanografia Física Costeira:</p>

            <p>Somos uma ferramenta gratuita e popular que conecta o usuário com o mar para auxiliar na antecipação de tomadas de decisões para prática de atividades náuticas e recreativas no litoral da Bahia. Para isto, oferecemos previsões e ciências do mar de forma popular para promover e facilitar o entendimento e a compreensão de como a dinâmica ambiental marinha pode impactar na vida, na segurança e no desempenho de praticantes de atividades náuticas e recreativas no litoral da Bahia.</p>
          
          <p>Por quê “previsões e ciências do mar”? Acreditamos que não adianta oferecer apenas previsões ambientais sem auxiliar na leitura, na interpretação e na contextualização destas previsões. Com base em dados ambientais históricos, estatísticos e trabalhos científicos de forma popular, o usuário será capaz de enxergar condições ambientais “normais”, “adversas” e “extremas”, além de aprender de forma lúdica a ciência por trás dos processos oceânicos e atmosféricos.</p>
          </section>

          <section className={s.iconsContainer}>
            <div className={s.iconsBar}>
            <IconWithText title="Integrado" text="Tudo que você precisa saber antes de sair de casa" imgSrc={"integrado"}></IconWithText>
            <IconWithText title="Didático" text="Ciência e estatística do ambiente" imgSrc={"didatico"}></IconWithText>
            <IconWithText title="Popular" text="Leitura de forma fácil para todos" imgSrc={"popular"}></IconWithText>
            <IconWithText title="Transparente" text="Tudo sobre os dados, validações e métodos" imgSrc={"transparente"}></IconWithText>
            </div>
          </section>

          <section ref={contactRef} className={s.creatorContainer}>
            <h4>Idealizador</h4>
            <CreatorDisplay imgSrc="rafael-mariani" socialMediaLogoSrc="sadsa" name="Rafael Mariani" ocupation="Oceanógrafo e Mestre em Oceanografia Física pela Universidade Federal da Bahia."></CreatorDisplay>
          </section>

          <section ref={blogRef} className={s.blogContainer}>
            <h4>Blog</h4>
            <span>Em construção</span>
          </section>

          <section className={s.popularityContainer}>
            <h4>O que dizem sobre nós</h4>
            <p>Confira o que os usuários da <strong>bahiamares</strong> estão achando da nossa plataforma.</p>
            <AliceCarousel autoPlayInterval={5000} duration={100} autoPlay buttonsDisabled mouseTrackingEnabled responsive={{
              1024:{
                items: 3
              },
              768:{
                items: 1
              }
            }} >
              <UserComments rating={5} comment="Sensacional! Excelente. Gostei bastante da plataforma" userName="Barbara Malias" profession="Professora"></UserComments>
              <UserComments rating={5} comment="Sensacional! Excelente. Gostei bastante da plataforma" userName="Barbara Malias" profession="Professora"></UserComments>
              <UserComments rating={5} comment="Sensacional! Excelente. Gostei bastante da plataforma" userName="Barbara Malias" profession="Professora"></UserComments>
              <UserComments rating={5} comment="Sensacional! Excelente. Gostei bastante da plataforma" userName="Barbara Malias" profession="Professora"></UserComments>
            </AliceCarousel>
          </section>

          <section className={s.sponsorsContainer}>
            <h4>Nossos patrocinadores</h4>
            <p>Em breve</p>
            <Button onClick={() => setSponsorModalVisible(true)} className={s.bluebtn}>Quero ser um patrocinador</Button>
          </section>

          <section className={s.partnersContainer}>
            <h4>Nossos parceiros</h4>
            <p>Em breve</p>
            <Button onClick={() => setPartnerModalVisible(true)} className={s.bluebtn}>Quero ser um parceiro</Button>
          </section>

          <ScrollTopButton onClick={() => scrollToTop()} />

          <Footer setFeedbackModal={() => setFeedbackModalVisible(true)}></Footer>
      </div>
 </main>
  )
}



export default MainContainer;
