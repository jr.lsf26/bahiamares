import * as React from "react";
import 'antd/dist/antd.css';

import MainContainer from '../containers/MainLandingPageContainer';
import { Router } from '@reach/router';
import PrevisoesPage from './previsoes';
import { ConfigProvider, LocaleProvider } from 'antd';
import ptBR from 'antd/es/locale/pt_BR';

import * as moment from 'moment';
import 'moment/locale/pt-br';

moment.localeData('pt-br');


const IndexPage = () => {
   return  <>

    <LocaleProvider locale={ptBR}>
    <ConfigProvider locale={ptBR}>
        <Router >
           
            <MainContainer path='/'></MainContainer>
            <PrevisoesPage path='/previsoes/:region/:forecast'></PrevisoesPage>
        </Router>
        </ConfigProvider>
        </LocaleProvider>
  
</>
};

export default IndexPage;