import * as React from "react";

import s from './style.module.scss';
import ForecastSection from '../containers/ForecastSection';
import { RouteComponentProps } from '@reach/router';
import { HeaderForecast } from '../components/HeaderForecast';

interface IProps extends RouteComponentProps{
    region?: string;
    forecast?: string;
}

const PrevisoesPage = (props: IProps) => (
    <div className={s.mainContainer}>
        <HeaderForecast test={props.region!}></HeaderForecast>
        <ForecastSection forecast={props.forecast ? props.forecast :  "ALERTA"} region={props.region!}></ForecastSection>
    </div>
);

export default PrevisoesPage;