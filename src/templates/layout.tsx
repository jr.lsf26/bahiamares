import * as React from "react";
import { StaticQuery, graphql } from "gatsby";

import s from "./style.module.scss";

export const Layout: React.FunctionComponent = ({ children }) => (
	<StaticQuery
		query={graphql`
			query SiteTitleQuery {
				site {
					siteMetadata {
						title
					}
				}
			}
		`}
		render={() => (
			<>
				<main className={s.container}>{children}</main>
				<footer>
					
				</footer>
			</>
		)}
	/>
);
