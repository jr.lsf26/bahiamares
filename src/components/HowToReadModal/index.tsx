import * as React from 'react';

import style from './style.module.scss';
import { Modal, Icon, Spin } from 'antd';
import { RoundButton } from '../RoundButton';

interface IHowToReadModalProps { 
    page: number, 
    currentImg: string;
    onClose: () => void; 
    visible: boolean;
    forecastType: string;
    displayType: string;
    setPdfPage: (page: number) => void;
    loading: boolean;
}

export const HowToReadModal =({ page, currentImg, onClose, visible, forecastType, displayType, setPdfPage, loading} : IHowToReadModalProps) => { 
    return (
        
        <Modal
        className={style.modalinfo}
        title={`Como interpretar -  ${forecastType.toLocaleLowerCase()}  ${displayType.toLocaleLowerCase()}`}
        maskClosable={true}
        visible={visible}
        onCancel={onClose}
        onOk={onClose}
        cancelButtonProps={
            {
                hidden: true
            }
        }
        >
            <Spin spinning={loading}>
            <div className={style.modalContent}>
            <img
                className={style.helpImg}
                 src={`data:application/png;base64, ` + currentImg}
              ></img>
            
            </div>
            </Spin>
            <div className={style.buttonsDiv}>
                {page > 1 && <RoundButton onClick={() => setPdfPage(page-1)} icon={<Icon className={style.btnIcon}  type="left"/>}/>}
                {page <= 5 && <RoundButton onClick={() => setPdfPage(page+1)} icon={<Icon className={style.btnIcon} type="right"/>}/>}
            </div>
        </Modal>
    )
}
