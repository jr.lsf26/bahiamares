import * as React from 'react';

import style from './style.module.scss';

interface IRoundButtonProps { 
    onClick: () => void;
    icon: React.ReactNode;
}

export const RoundButton = ({onClick, icon} : IRoundButtonProps) => { 

    return ( 
        <>
        <div className={style.roundBtn} onClick={onClick}>
        {icon}
        </div>
        </>
    )

}