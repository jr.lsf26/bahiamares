import * as React from 'react';

import s from './style.module.scss';

interface IProps{
    imgSrc: string;
    title: string; 
    content: string;
}
    
const Info = (props: IProps) => {
    return(
        <div className={s.infoBox}>
        <img className={s.icon} src={require("../../assets/images/" + props.imgSrc + ".png")}></img>
        <h4>{props.title}</h4>
        <p>{props.content}</p>
    </div>
    )
}
   
export default Info;