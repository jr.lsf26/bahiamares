import * as React from 'react';

import style from './style.module.scss';
import { Modal, Form, Row, Col, Select } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { regions } from '../Navbar';


interface IChangeRegionFormProps extends FormComponentProps { 
    onSubmit: (values: any) => void;
    visible: boolean;
    closeModal: () => void;
}

export const InnerChangeRegionForm = ({onSubmit, visible, closeModal, form, ...rest}: IChangeRegionFormProps) => {

    const {getFieldDecorator, validateFields} = form;

    const handleSubmit = (e: React.FormEvent) => { 
        e.preventDefault();
        validateFields((errors, values: any) => { 
            if(!errors){
                onSubmit(values)
            }
        })
    }

    return(
        <Modal
        visible={visible}
        title="Selecione uma Região"
        okText="Selecionar"
        onCancel={closeModal}
        maskClosable={true}
        onOk={handleSubmit}
        destroyOnClose={true}
        cancelText="Cancelar"
        >

            <Form className={style.form} layout="vertical" onSubmit={handleSubmit} {...rest}>
                <Row gutter={0}>
                    <Col span={24}>
                        <Form.Item className={style.region} label="Condições do mar e do tempo em:">
                                {getFieldDecorator("region", {
                                    rules: [
                                        {message: "Selecione uma região!",
                                        required: true
                                    }
                                    ]
                                })
                                (
                                    <Select>
                                        {regions.map(region =>  
                                          (  <Select.Option key={region} value={region}>
                                                {region}
                                            </Select.Option>
                                            )
                                        )}
                                    </Select>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Modal>
    )

 }

 export const ChangeRegionForm = Form.create<IChangeRegionFormProps>({name: "changeregion_form"})(InnerChangeRegionForm);