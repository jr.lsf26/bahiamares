import * as React from 'react';
import style from './style.module.scss';
import StarRatingComponent from 'react-star-rating-component';

interface IUserCommentsProps{
    comment: string;
    userName: string;
    profession: string;
    rating: number;
}

export const UserComments = ({comment, userName, profession, rating} : IUserCommentsProps) => { 

    return (
        <div className={style.commentBox}>
            <span>{`"${comment}"`}</span>
            <div className={style.rounded}/>
            <div className={style.rateAndName}>
            <StarRatingComponent 
            editing={false} 
            name="rating"
            starCount={5}
            value={rating}
            />
            <span>- <strong>{userName}</strong> {profession}</span>
            </div>
        </div>
    )

}