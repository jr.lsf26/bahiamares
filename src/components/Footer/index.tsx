import * as React from 'react';

import style from './style.module.scss';
import { Button, Modal} from 'antd';
import { SocialMediaIconButton } from '../SocialMediaIconButton';
import { ContactForm } from '../ContactForm';

interface IFooterProps {
 setFeedbackModal: (value: boolean) => void;
}

export const Footer = ({setFeedbackModal}: IFooterProps) => { 

    const confirm = Modal.confirm;

    const openModal = () => {
        confirm({
            title: "Importante",
            content: (
                <div>
                    <p>{"Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos."}</p>
                    <p>{"Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI."}</p>
                </div>
            ),
            okText: "Ok",
            cancelText: "Fechar",
            maskClosable: true,
            width: "40%",
            icon: null
        })
    }

    return (
    <footer className={style.footer}>
        <div className={style.upperBox}>
        <div className={style.firstBox}>
            <img className={style.logo} src={require('../../assets/logo/logo-white.png')}/>
            <p>A plataforma bahiamares é idealizada por oceanógrafos</p>
            <p><strong>E-mail: </strong>contato@bahiamares.com.br</p>
            <p><strong>WhatsApp: (71) 99401-2942</strong></p>
            <div className={style.socialMediaLogos}>
                <SocialMediaIconButton link="http://www.instagram.com/bahiamares" imgType="instagram"/>
                <SocialMediaIconButton link="https://www.facebook.com/bahiamares.com.br" imgType="facebook"/>
                <SocialMediaIconButton link="/" imgType="twitter"/>
                <SocialMediaIconButton link="https://www.youtube.com/channel/UCmvfK7PKKo2M_vT3EI--kNQ?view_as=public" imgType="youtube"/>
            </div>
        </div>
        <div className={style.secondBox}>
            <h4>Como podemos melhorar?</h4>
            <Button onClick={() => setFeedbackModal(true)} className={style.feedbackBtn}>Deixe seu feedback!</Button>
        </div>
        <div className={style.thirdBox}>
            <h4>Receba nossas notificações!</h4>
           <ContactForm />
            <a onClick={() => openModal()}>Importante</a>
        </div>
        </div>
        <div className={style.lowerBox}>
            <p className={style.bahiamaresTxt}>© 2020 bahiamares</p>
            <p className={style.gersonTxt}>Personalizado por <a>Gerson Barbosa.</a></p>
        </div>
        
        
    </footer>)

}