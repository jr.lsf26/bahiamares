import * as React from "react";
import { ButtonProps } from "antd/lib/button";

import s from "./style.module.scss";

interface IProps extends ButtonProps {
  disable?: boolean;
  img: string;
  tooltip?: string;
  isSelected?: boolean;
  hasLabel?: boolean;
}

const IconButton = (props: IProps) => {
  return (
    <button
      onClick={props.onClick}
      className={props.isSelected ? s.focusedButton : s.button}
      disabled={props.disable}
    >
      <img
        className={props.disable ? s.disabled : s.icon}
        src={require("../../assets/images/" + props.img + ".png")}
      ></img>
      {props.hasLabel && (<p className={s.text}>Alterar data/hora</p>)}
    </button>
  );
};

export default IconButton;
