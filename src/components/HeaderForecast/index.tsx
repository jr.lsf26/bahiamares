import * as React from 'react';

import style from './style.module.scss';
import { NavbarForecast } from '../NavbarForecast';
import { navigate } from '@reach/router';
import { Dropdown, Icon, Menu } from 'antd';
import { regionFromLocation, forecastFromLocation } from '../../utils/locationPath';
import { regionsName } from '../../utils/regions';
import { regionsVariables, regions, forecastsVariables } from '../Navbar';


interface IHeaderForecastProps {
    test: string;
}

export const HeaderForecast = ({test}: IHeaderForecastProps) => {

    const[curWidth, setCurWidth] = React.useState();
    const [arrLocation, setArrLocation] = React.useState<any>([]);

    console.log(test)
    const forecasts = [
        "Alerta Oceânico", "Ondas", "Vento", "Maré", "Calendário Lunar", "Correntes", "Tempo", "Qualidade da água", "Temp. da Superfície do Mar", "SOS"
      ]
      const menuForecast = ( 
        <Menu mode="vertical">
                {forecasts.map(forecast => (
            <Menu.Item key={forecast}>
            <div onClick={() => changeForecast(forecast)}> 
           <> {forecast}</>
            </div>
          </Menu.Item>
          ))}
    
        </Menu>
        
      )

    const changeRegion = (region: string) => { 
      navigate(`/previsoes/${regionsVariables[region.replace(/\s/g, "")]}/${forecastFromLocation(arrLocation)}`)
        
      }

      const changeForecast = (forecast: string) => { 
        navigate(`/previsoes/${regionFromLocation(arrLocation)}/${forecastsVariables[forecast.replace(/\s/g, "")]}`)
        
      }

    const menuRegions = ( 
        <Menu id={style.regionBtn} mode="vertical">
                {regions.map(region => (
            <Menu.Item className={style.regionBtn} id={style.regionBtn} key={region}>
            <div onClick={() => changeRegion(region)} > 
           <> {region}</>
            </div>
          </Menu.Item>
          ))}
        </Menu>
      )
      React.useEffect(() => {
          setCurWidth(window.innerWidth)
          setArrLocation(window.location.pathname.split('/'))
      },[])
      
    return (
        <header className={style.header}>
            <div className={style.container}>
            <img onClick={() => navigate('/')} className={style.logo} src={require("../../assets/logo/logo-bahiamares.png")}></img>
            {curWidth < 769 && ( 
                <Dropdown className={style.dropdownBtn} trigger={['click']} overlay={menuRegions}>
                <a className={style.text}>
                 {regionsName[regionFromLocation(arrLocation)]}
                 <Icon type="down"></Icon>
                 </a>
            </Dropdown>
            )}
            <NavbarForecast region={test}></NavbarForecast>
            </div>
            {curWidth < 769 && ( 
                <div className={style.lowerResponsiveBox}>
                    <Dropdown overlay={menuForecast}>
                            <a className={style.text}>
                            Preveja
                            <Icon type="down"></Icon>
                            </a>
                            
                        </Dropdown>
                </div>
            )}
        </header>
    )

}