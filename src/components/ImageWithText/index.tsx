import * as React from 'react';

import style from './style.scss';

interface IImageWithTextProps{
    imgSrc: string;
    text: string;
}

export const ImageWithText = (props: IImageWithTextProps) => { 


    return ( 
        <div className={style.container}>
            <img className={style.image} src={require(`../../assets/icons/${props.imgSrc}.png`)}></img>
    <p>{props.text}</p>
        </div>
    )
}