import * as React from 'react';

import style from './style.module.scss';

interface IForecastSelectionProps{
    onForecastClick: (forecast: string) => void;
}

export const ForecastSelection = ({onForecastClick}: IForecastSelectionProps) => { 


    const forecastOptions = [
        {
            title: "sad",
            icon: "alert",
            type: "ALERTA"
        }
    ]

    return (<div className={style.box}>
        <div className={style.title}>
            <p>Preveja</p>
        </div>

        <div className={style.options}>
            {forecastOptions.map(option => (
                <div onClick={() => onForecastClick(option.type)} className={style.optionBox}>
                    <img></img>
                    <span></span>
                </div>
            ))}
        </div>

    </div>)
}