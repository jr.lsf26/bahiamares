import * as React from 'react';

import style from './style.module.scss';
import { Menu, Dropdown, Icon } from 'antd';
import { navigate} from 'gatsby';
import { regionsVariables, regions, forecastsVariables } from '../Navbar';
import { regionFromLocation, forecastFromLocation } from '../../utils/locationPath';
import { regionsName } from '../../utils/regions';

interface IProps {
  region: string
}

export const NavbarForecast = ({region} : IProps) => { 

  const [arrLocation, setArrLocation] = React.useState<any>([]);


    const forecasts = [
        "Alerta Oceânico", "Ondas", "Vento", "Maré", "Calendário Lunar", "Correntes", "Tempo", "Qualidade da água", "Temp. da Superfície do Mar", "SOS"
      ]
      const menuForecast = ( 
        <Menu mode="vertical">
                {forecasts.map(forecast => (
            <Menu.Item key={forecast}>
            <div onClick={() => changeForecast(forecast)}> 
           <> {forecast}</>
            </div>
          </Menu.Item>
          ))}
    
        </Menu>
        
      )

      const changeRegion = (region: string) => { 
        navigate(`/previsoes/${regionsVariables[region.replace(/\s/g, "")]}/${forecastFromLocation(arrLocation)}`)
        // setTimeout(() => {
        //   window.location.reload()
        // }, 50);
      }

      const changeForecast = (forecast: string) => { 
        navigate(`/previsoes/${regionFromLocation(arrLocation)}/${forecastsVariables[forecast.replace(/\s/g, "")]}`)
        // setTimeout(() => {
        //   window.location.reload()
        // }, 50);
      }

      const menuRegions = ( 
        <Menu mode="vertical">
                {regions.map(region => (
            <Menu.Item key={region}>
            <div onClick={() => changeRegion(region)} > 
           <> {region}</>
            </div>
          </Menu.Item>
          ))}
        </Menu>
      )

      React.useEffect(() => {
        setArrLocation(window.location.pathname.split('/'))
    },[])

    return (
<Menu className={style.menu} mode="horizontal" theme="dark">
    <Menu.Item>
      <Dropdown overlay={menuForecast}>
        <a className={style.text}>
          Preveja
          <Icon type="down"></Icon>
        </a>
        
      </Dropdown>
    </Menu.Item>
    <Menu.Item>
      <Dropdown className={style.dropdownBtn} trigger={['click']} overlay={menuRegions}>
        <a className={style.textRegion}>
          {/* {regionsName[regionFromLocation(arrLocation)]} */}
          {regionsName[region]}
          <Icon type="down"></Icon>
        </a>
      </Dropdown>
    </Menu.Item>
    </Menu>
    )
}