import * as React from 'react';

import style from './style.module.scss';

interface IBannerProps{
    forecast: string;
}

export const ForecastNavigationBanner = ({forecast} : IBannerProps) => { 

    return (
        <div className={style.banner}>
            <div className={style.box}>
                <p>Você está navegando em:</p>
                <h3>{forecast === "ALERTA" ? "ALERTA OCEÂNICO" : forecast}</h3>
            </div>
        </div>
    )
}