import * as React from 'react';

import s from "../style.module.scss";

export const WaveMapContents = () => { 

    return ( 
        <div className={s.contentBox}>
            <img className={s.image} src={require("../../../assets/howToImgs/wave-map-howto.png")}></img>
            <p>Este mapa é importante para visualizar a formação e propagação de
grandes ondulações (ondas de tempestades ou swell*) no oceano
Atlântico. É possível observar se as grandes ondulações chegarão junto a
costa ou se passarão por fora em águas profundas em mar aberto.</p>
            <p>*Ondas de SWELL são ondas que se formaram em tempestades bem
longe da praia, que viajaram milhares de quilômetros pelo oceano, se
distanciando dos locais de ventania aonde elas foram criadas. Essas ondas
chegam com maior energia na costa e apresentam uma assinatura de
período de onda acima do valor de 10 segundos.</p>
            <img className={s.image} src={require("../../../assets/howToImgs/wave-map-img.png")}></img>
        </div>
    )
}