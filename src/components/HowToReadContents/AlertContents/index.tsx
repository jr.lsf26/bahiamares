import * as React from 'react';

import s from "../style.module.scss";


export const AlertMapContents = () => { 
    return (
        <div>
            <img className={s.image} src={require("../../../assets/howToImgs/alert-map-howto.png")}></img>
        </div>
    )
}