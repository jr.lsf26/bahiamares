import * as React from 'react';

import s from "../style.module.scss";


export const TideContents = () => { 
    return (
        <div>
<img  className={s.image} src={require("../../../assets/howToImgs/tide-graf-howto.png")}></img>
            <p>Note que a amplitude entre a maré toda alta e a maré toda baixa se torna
maior nas fases de lua cheia e nova, enquanto que se torna menor nas fases
de lua minguante e crescente. Conforme a figura abaixo, nas fases de lua cheia
e nova existe um alinhamento do sistema Terra, lua e Sol, favorecendo a
amplificação da maré. Também chamado de maré de sizígia (popularmente
“maré viva”). Já nas fases de lua minguante e crescente existe um
desalinhamento entre o sistema Terra, lua e Sol, que não favorece a
amplificação da maré. Também chamado de maré de quadratura
(popularmente “maré morta”). Por isso, fique atento com o calendário lunar.</p>
            <p>A maré nessa região é classificada semi-diurna, ou seja, apresenta diariamente
2 momentos de maré toda alta e 2 momentos de maré toda baixa. Note que
os horários da maré toda alta e toda baixa avançam cerca de 40-50 minutos a
cada dia. Isso ocorre por conta do deslocamento da lua em torno da Terra.</p>
            <img  src={require("../../../assets/howToImgs/tide-graf-img.png")}></img>
        </div>
    )
}