import * as React from 'react';

import s from "../style.module.scss";


export const CurrentsMapContents = () => {
    return (
        <div>
            <img className={s.image} src={require("../../../assets/howToImgs/current-map-howto.png")}></img>
        </div>
    )
}

export const CurrentsGrafContents = () => {
    return (
        <div>
            <img className={s.image} src={require("../../../assets/howToImgs/current-graf-howto.png")}></img>
            <p>Note que as correntes de maré são mais intensas nas fases de lua cheia e nova,
enquanto que se tornam mais fracas nas fases de lua minguante e crescente.
Conforme a figura abaixo, nas fases de lua cheia e nova existe um alinhamento do
sistema Terra, lua e Sol, favorecendo a amplificação da maré e, consequentemente, a
intensificação de correntes de maré. Também chamado de maré de sizígia
(popularmente “maré viva”). Já nas fases de lua minguante e crescente existe um
desalinhamento entre o sistema Terra, lua e Sol, que não favorece a amplificação da
maré e, consequentemente, as correntes de maré são mais fracas. Também chamado
de maré de quadratura (popularmente “maré morta”). Por isso, fique atento com o
calendário lunar.</p>
            <p>Observe que no período da maré toda alta e toda baixa as correntes de maré tendem
a enfraquecer (próximo a zero km/h). Neste momento de transição, conhecido como
“estofa da maré”, é quando ocorre as menores velocidades de correntes de maré. Em
contrapartida, no período da metade do ciclo de maré de enchente ou vazante
(conhecido como meia-maré), é quando as correntes de maré atingem velocidades
máximas. Por isso, fique atento com os horários da maré.</p>
            <img className={s.image} src={require("../../../assets/howToImgs/tide-graf-img.png")}></img>
        </div>
    )
}