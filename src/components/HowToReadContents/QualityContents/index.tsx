import * as React from 'react';

import s from "../style.module.scss";

export const QualityContents = () => { 
    return (
        <div>
            <img className={s.image}  src={require("../../../assets/howToImgs/qualidade-howto.png")}></img>
            <img className={s.image}  src={require("../../../assets/howToImgs/qualidade-howto-info.png")}></img>
            <img className={s.image}  src={require("../../../assets/howToImgs/qualidade-howto-footer.png")}></img>
        </div>
    )
}