import * as React from 'react';

import s from "../style.module.scss";


export const SinoticaContents = () => { 

    return ( 
        <div>
            <img className={s.image} src={require("../../../assets/howToImgs/sinotica-howto.png")}></img>
        </div>
    )
}