import * as React from 'react';

import s from "../style.module.scss";


export const WindContents = () => { 
    return (
        <div>
           <img className={s.image} src={require("../../../assets/howToImgs/wind-map-howto.png")}></img>
           <p>Esse mapa é ideal para visualizar de onde vem e com qual intensidade
o vento chegará em determinada região.</p>
        </div>
    )
} 