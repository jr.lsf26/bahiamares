import * as React from 'react';

import style from './style.module.scss';
import { Icon } from 'antd';

interface IScrollTopProps { 
    onClick: () => void;
}

export const ScrollTopButton = ({onClick} : IScrollTopProps) => { 

    return ( 
        <>
        <div className={style.roundBtn} onClick={onClick}>
            <Icon className={style.icon} type="up"/>
        </div>
        </>
    )

}