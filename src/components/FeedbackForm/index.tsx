import * as React from 'react';

import style from './style.module.scss';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { checkEmail } from '../../utils/formatters';
import TextArea from 'antd/lib/input/TextArea';


interface IFeedbackFormProps extends FormComponentProps { 
    onSubmit: (values: any) => void;
    visible: boolean;
    closeModal: () => void;
}

export const InnerFeedbackForm = ({onSubmit, visible, closeModal, form, ...rest}: IFeedbackFormProps) => {

    const {getFieldDecorator, getFieldsValue, setFieldsValue} = form;

    const handleSubmit = (e: React.FormEvent) => { 
        setFieldsValue({type: 'Feedback'})
        e.preventDefault();
        onSubmit(getFieldsValue());
    }

    return(
        <Modal
        visible={visible}
        title="Fale conosco"
        okText="Enviar"
        onCancel={closeModal}
        maskClosable={true}
        onOk={handleSubmit}
        destroyOnClose={true}
        cancelText="Fechar"
        >

            <Form className={style.form} layout="vertical" onSubmit={handleSubmit} {...rest}>
                <Row gutter={8}>
                    <Col span={12}>
                        <Form.Item >
                                {getFieldDecorator("name", {
                                    rules: [
                                        {message: "Nome é obrigatório!",
                                        required: true
                                    }
                                    ]
                                })
                                (
                                    <Input placeholder={"Seu nome"}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item >
                                {getFieldDecorator("email", {
                                    rules: [
                                        {message: "Nome é obrigatório!",
                                        required: true
                                    },{
                                        message: "Insira um e-mail em um formato válido!",
                                        validator: checkEmail
                                    }
                                    ]
                                })
                                (
                                    <Input placeholder={"E-mail"}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    <Form.Item >
                                {getFieldDecorator("phone", {
                                })
                                (
                                    <Input placeholder={"Telefone"}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    <Form.Item >
                                {getFieldDecorator("message", {
                                })
                                (
                                    <TextArea placeholder={"Como podemos ajudar?"}></TextArea>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    <Form.Item >
                                {getFieldDecorator("type", {
                                })
                                (
                                    <Input  disabled className={style.hiddenInput}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Modal>
    )

 }

 export const FeedbackForm = Form.create<IFeedbackFormProps>({name: "feedback_form"})(InnerFeedbackForm);