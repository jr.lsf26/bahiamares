import * as React from "react";
import ServiceType from '../ServiceType';

import s from './style.module.scss';

interface IServiceListProps{
    handleClick: (selected: string) => void;
}

const ServiceList= ({handleClick}: IServiceListProps) => (
<div className={s.container}>
        <ServiceType handleClick={handleClick} serviceName={"Surf"} imgSrc="surf"></ServiceType>
        <ServiceType handleClick={handleClick}  serviceName={"Kite-surf"}  imgSrc="kitesurf"></ServiceType>
        <ServiceType handleClick={handleClick}  serviceName={"Mergulho"}  imgSrc="diving"></ServiceType>
        <ServiceType handleClick={() => handleClick("slalom")} serviceName={"Canoagem"}  imgSrc="slalom"></ServiceType>
        <ServiceType handleClick={() => handleClick("photo")} serviceName={"Fotografia"}  imgSrc="photography"></ServiceType>
        <ServiceType handleClick={() => handleClick("drone")} serviceName={"Drone"}  imgSrc="drone"></ServiceType>
        <ServiceType handleClick={() => handleClick("iate")} serviceName={"Passeio"}  imgSrc="passeio-iate"></ServiceType>
        <ServiceType handleClick={() => handleClick("fish")} serviceName={"Naútica"}  imgSrc="nautica"></ServiceType>
        <ServiceType handleClick={() => handleClick("sup")} serviceName={"SUP"}  imgSrc="standup-paddle"></ServiceType>
    </div>
)


export default ServiceList;