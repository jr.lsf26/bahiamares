import * as React from 'react';

import style from './style.module.scss';

interface IForecastInfoBarProps{ 
    title: string; 
    iconSrc: string;
    onClick: () => void;
}



export const ForecastInfoBar = ({title, iconSrc, onClick}: IForecastInfoBarProps) => { 

    return (
        <div onClick={onClick} className={style.bar}>
            <img className={style.logo} src={require(`../../assets/icons/forecastIcons/${iconSrc}.png`)}/>
            <label>{title}</label>
        </div>
    )
}