import * as React from 'react';

import style from './style.module.scss';
import { Avatar } from 'antd';

interface ICreatorDisplayProps{
    imgSrc: string;
    socialMediaLogoSrc?: string;
    name: string;
    ocupation: string;
}

export const CreatorDisplay = ({imgSrc, name, ocupation}: ICreatorDisplayProps) => { 

    return (
        <div className={style.creatorBox}>
            <Avatar src={require(`../../assets/icons/team/${imgSrc}.jpg`)} className={style.avatar}></Avatar>
            <a href="https://www.linkedin.com/in/rafael-mariani-49a558157/">{name}</a>
            <span>{ocupation}</span>
        </div>
    )
}