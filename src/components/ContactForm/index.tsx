import * as React from 'react';

import style from './style.module.scss';
import Form, { FormComponentProps } from 'antd/lib/form';
import { Button, Icon } from 'antd';
import { checkEmail } from '../../utils/formatters';

interface IContactFormProps extends FormComponentProps { 
}

const InnerContactForm = ({ form}: IContactFormProps) => {

    const { getFieldDecorator, validateFields } = form;

    const handleSubmit = (e: React.FormEvent) => { 
        e.preventDefault();

		validateFields((errors, values: any) => {
			if (!errors) {
				console.log(values)
			}
		});

    }

    return (
        <Form className={style.contactForm} onSubmit={handleSubmit}>
        <Form.Item label="Insira seu e-mail">
            {
                getFieldDecorator("email", { 
                    
                    rules: [
                        {
                            message: "Informe o e-mail!",
                            required: true
                        },
                        {
                            message: "Digite um e-mail válido!",
                            validator: checkEmail
                        }
                    ]
                })(<input placeholder="Seu e-mail:" type="email" className={style.input} />)
            }
        </Form.Item>
        <Icon className={style.mailIcon} type="mail"></Icon>

        <Form.Item>
            <Button className={style.submitBtn} htmlType="submit">Cadastrar</Button>
        </Form.Item>
    </Form>
    )
}

export const ContactForm = Form.create<IContactFormProps>({name: "contact_form"})(InnerContactForm);