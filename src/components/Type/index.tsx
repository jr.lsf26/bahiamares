import * as React from "react";
import s from './style.module.scss';
import { Tooltip } from 'antd';

interface IProps{
    title: string,
    imgSrc: string,
    class?: boolean
}


const ForecastType = (props: IProps) => (
    <Tooltip title={props.title}>
    <div className={s.button}>
        <img className={props.class ? s.smallIcon : s.icon} src={require('../../assets/images/' + props.imgSrc + '.png')}></img>
    </div>
    </Tooltip>
    
)

export default ForecastType;