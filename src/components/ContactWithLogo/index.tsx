 import * as React from 'react';
import { Icon } from 'antd';

import style from './style.module.scss';

export interface IContact{
    logo: string;
    contact: string;
}

interface IProps{
    contactObj: IContact
}

export const ContactWithLogo = ({contactObj} : IProps) => { 
    return (
        <div className={style.contactBox}>
            <Icon className={style.smallLogo} type={contactObj.logo}/>
            <p>{contactObj.contact}</p>
        </div>
    )
}