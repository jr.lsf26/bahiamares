import * as React from 'react';

import s from './style.module.scss';
import Navbar from '../Navbar';
import { navigate } from '@reach/router';
import { Menu, Dropdown, Icon } from 'antd';
import { Link } from 'gatsby';


interface IProps {
  onClickForecast: (forecast: string) => void;
  scrollToAbout: () => void;
  scrollToBlog: () => void;
  openContactModal: () => void;
}

export const Header = ({onClickForecast, scrollToAbout, scrollToBlog, openContactModal}: IProps) => { 

  const [menuOpen, setMenuOpen] = React.useState(false);
  const [isOnTop, setIsOnTop] = React.useState(false);

const handleScroll = () => {
    setIsOnTop(window.scrollY <= 0);
}

  React.useEffect(() =>  {
  window.addEventListener("scroll", handleScroll);
    handleScroll();
},[isOnTop])

const forecasts = [
  "Alerta Oceânico", "Ondas", "Vento", "Maré", "Calendário Lunar", "Correntes", "Tempo", "Qualidade da água", "Temp. da Superfície do Mar", "SOS"
]

const menu = ( 
  <Menu mode="vertical">

          {forecasts.map(forecast => (

      <Menu.Item onClick={() => onClickForecast(forecast)} key={forecast}>
        {forecast}
    </Menu.Item>
    ))}

  </Menu>
  
)

  return (
    <header className={!isOnTop ? s.scrolled : s.header}>
      <div className={s.container}>
      <img onClick={() => navigate("/")} className={s.logo} src={require("../../assets/logo/logo-bahiamares.png")}></img>
    <Navbar scrollToAbout={scrollToAbout} scrollToBlog={scrollToBlog} openContactModal={openContactModal} onClickForecast={onClickForecast}></Navbar>
    <div className={s.menudropdown}>
    <div
      onClick={() => setMenuOpen(!menuOpen)}
      className={menuOpen ? s.linesOpen : s.lines}
    >
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
  
      </div>
      <div className={menuOpen ? s.whiteDiv : s.hidden}>
      <Menu className={s.menu} mode="vertical" >
    <Menu.Item>
      <Link className={s.text} to={`/`}>Home</Link>
    </Menu.Item>
    <Menu.Item className={s.transparent}>
      <Dropdown overlay={menu}>
        <a className={s.text}>
          Preveja
          <Icon type="down"></Icon>
        </a>
        
      </Dropdown>
    </Menu.Item>
   
    <Menu.Item>
      <a onClick={scrollToAbout}>Sobre</a>
    </Menu.Item>
    <Menu.Item>
      <a onClick={scrollToBlog}>Blog</a>
    </Menu.Item>
    <Menu.Item>
      <a onClick={openContactModal}>Contato</a>
    </Menu.Item>
    </Menu>
      </div>
</header>
)


}
