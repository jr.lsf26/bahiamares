import * as React from 'react';

import style from './style.module.scss';
import ForecastType from '../Type';

interface IForecastBarProps{ 
    setForecastType: (forecast: string) => void;
    setServiceDisplay: (show: boolean) => void;
    forecast: string;
}

export const MobileForecastBar = ({setForecastType, setServiceDisplay, forecast }: IForecastBarProps) => { 

    return (
        <div className={style.bar}>
            <div
             className={forecast === "ALERTA" ? style.focus : style.serviceBtn}
             onClick={() => {
                setForecastType("ALERTA");
                setServiceDisplay(false);
              }}
            >
              <ForecastType
              class
                title="Alerta Oceânico"
                imgSrc="alert"
              ></ForecastType>
            </div>
            <div
             className={forecast === "ONDAS" ? style.focus : style.serviceBtn}
             onClick={() => {
                setForecastType("ONDAS");
                setServiceDisplay(false);
              }}
            >
              <ForecastType class title="Ondas" imgSrc="wave"></ForecastType>
            </div>
            <div
             className={forecast === "VENTO" ? style.focus : style.serviceBtn}
              onClick={() => {
                setForecastType("VENTO");
              }}
            >
              <ForecastType class title="Vento" imgSrc="wind"></ForecastType>
            </div>
            <div
             className={forecast === "MARE" ? style.focus : style.serviceBtn}
             onClick={() => {
                setForecastType("MARE");
              }}
            >
              <ForecastType class title="Maré" imgSrc="tide"></ForecastType>
            </div>
            <div
             className={forecast === "CORRENTES" ? style.focus : style.serviceBtn}
             onClick={() => {
                setForecastType("CORRENTES");
              }}
            >
              <ForecastType
              class
                title="Correntes"
                imgSrc="currents-icon"
              ></ForecastType>
            </div>
            <div
              className={forecast === "TEMPO" ? style.focus : style.serviceBtn}
              onClick={() => {
                setForecastType("TEMPO");
              }}
            >
              <ForecastType class title="Tempo" imgSrc="weather"></ForecastType>
            </div>
            <div
              className={forecast === "QUALIDADE" ? style.focus : style.serviceBtn}
              onClick={() => {
                setForecastType("QUALIDADE");
              }}
            >
              <ForecastType class
                title="Qualidade da Água"
                imgSrc="water-quality"
              ></ForecastType>
            </div>
            <div
              className={forecast === "TSM" ? style.focus : style.serviceBtn}
              onClick={() => {
                setForecastType("TSM");
              }}
            >
              <ForecastType class
                title="Temperatura da Superfície do Mar"
                imgSrc="water-temperature"
              ></ForecastType>
            </div>
            <div
              className={forecast === "SOS" ? style.focus : style.serviceBtn}
              onClick={() => {
                setForecastType("SOS");
              }}
            >
              <ForecastType class title="SOS" imgSrc="boia"></ForecastType>
            </div>
        </div>
    )
}