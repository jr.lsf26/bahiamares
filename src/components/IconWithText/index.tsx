import * as React from 'react';

import style from './style.module.scss';

interface IIconWithTextProps{
    imgSrc: string;
    title: string;
    text: string;
}

export const IconWithText = (props: IIconWithTextProps) => { 


    return ( 
        <div className={style.container}>
            <img className={style.image} src={require(`../../assets/images/tags/${props.imgSrc}.png`)}></img>
            <h5 className={style.title}>{props.title}</h5>
            <p className={style.text}>{props.text}</p>
        </div>
    )
}