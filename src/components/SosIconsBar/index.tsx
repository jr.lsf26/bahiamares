import * as React from 'react';
import IconButton from "../../components/IconButton";
import s from './style.scss';


const SosIconsBar = () => { 
    return ( 
        <div className={s.box}>
             <IconButton tooltip="Avisos" img={"warning"}></IconButton>
             <IconButton  tooltip="Primeiros Socorros" img={"firstaid"}></IconButton>
             <IconButton  tooltip="Correntes de Retorno" img={"drowning"}></IconButton>
             <IconButton  tooltip="Água-viva" img={"jellyfish"}></IconButton>
             <IconButton  tooltip="Mais?" img={"more"}></IconButton>
        </div>
    )
}

export default SosIconsBar;