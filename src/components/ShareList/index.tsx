import * as React from 'react';

import style from './style.module.scss';
import { WhatsappShareButton, WhatsappIcon, FacebookShareButton, FacebookIcon, TwitterShareButton, TwitterIcon, TelegramShareButton, TelegramIcon } from 'react-share';

interface IShareListProps {
    url: string;
}

export const ShareList = ({url}: IShareListProps) => { 


    return (
        <div className={style.shareBox}>
                <WhatsappShareButton  className={style.button} url={url}>
                    <WhatsappIcon size={40} round={true} ></WhatsappIcon>
                </WhatsappShareButton>
                <FacebookShareButton className={style.button} url={url}>
                    <FacebookIcon size={40} round={true}></FacebookIcon>
                </FacebookShareButton>
                <TwitterShareButton className={style.button} url={url} >
                    <TwitterIcon size={40} round={true}></TwitterIcon>
                </TwitterShareButton>
                <TelegramShareButton className={style.button} url={url}>
                    <TelegramIcon size={40} round={true}></TelegramIcon>
                </TelegramShareButton>
        </div>
    )
}