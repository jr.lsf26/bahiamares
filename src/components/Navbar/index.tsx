import * as React from "react";

import { Menu, Dropdown, Icon} from 'antd';
import { Link } from 'gatsby';

import style from './style.module.scss';
// import IconButton from '../IconButton';

export const regions = [
  "Salvador", "Alcobaça", "Arembepe", "Baía de Todos os Santos", "Boipeba", "Camamu",
  "Canavieiras", 'Caravelas', "Conde", "Costa do Sauipe", 
  "Ilhéus", "Itacaré", "Lauro de Freitas", "Morro de São Paulo", "Mucuri", "Nova Viçosa",
  "Porto Seguro", "Prado", "Praia do Forte", "Santa Cruz Cabrália", "Trancoso", "Valença"
]

export const regionsVariables: {[key: string]: string} = {
  Salvador: "SSA",
  Alcobaça: "ALCO",
  Arembepe: "AREM",
 BaíadeTodososSantos: "BTS",
  Boipeba: "BOIP",
  Camamu: "CAMA",
  Canavieiras: "CANA",
  Caravelas: "CARAV",
  Conde: "COND", 
  CostadoSauipe: "SAUI",
  Ilhéus: 'ILHE',
  Itacaré: 'ITAC',
  LaurodeFreitas: 'LAUR',
  MorrodeSãoPaulo: 'MSP',
  Mucuri: "MUCU", 
  NovaViçosa: "NV",
  Prado: "PRAD",
  PortoSeguro: 'PS',
  PraiadoForte: 'PF',
  SantaCruzCabrália: "CABRA",
  Trancoso: 'TRANC',
  Valença: 'VAL'
}

export const forecastsVariables: {[key: string]: string} = {
  AlertaOceânico: "ALERTA",
  Ondas: "ONDAS", 
  Vento: "VENTO",
  Maré: "MARE",
  CalendárioLunar: "LUNAR", 
  Correntes: "CORRENTES",
  Tempo: "TEMPO",
  QualidadedaÁgua: "QUALIDADE", 
  TempdaSuperfíciedoMar: "TSM", 
  SOS: "SOS"
}

interface IProps { 
  onClickForecast: (forecast: string) => void;
  scrollToAbout: () => void;
  scrollToBlog: () => void;
  openContactModal: () => void;
}

const Navbar = ({onClickForecast, scrollToAbout, scrollToBlog, openContactModal}: IProps) => {
  const forecasts = [
    "Alerta Oceânico", "Ondas", "Vento", "Maré", "Calendário Lunar", "Correntes", "Tempo", "Qualidade da água", "Temp. da Superfície do Mar", "SOS"
  ]

  const menu = ( 
    <Menu mode="vertical">

            {forecasts.map(forecast => (

        <Menu.Item onClick={() => onClickForecast(forecast)} key={forecast}>
          {forecast}
      </Menu.Item>
      ))}

    </Menu>
    
  )

  return (
    <Menu className={style.menu} mode="horizontal" theme="dark">
    <Menu.Item>
      <Link className={style.text} to={`/`}>Home</Link>
    </Menu.Item>
    <Menu.Item>
      <Dropdown overlay={menu}>
        <a className={style.text}>
          Preveja
          <Icon type="down"></Icon>
        </a>
        
      </Dropdown>
    </Menu.Item>
   
    <Menu.Item>
      <a onClick={scrollToAbout}>Sobre</a>
    </Menu.Item>
    <Menu.Item>
      <a onClick={scrollToBlog}>Blog</a>
    </Menu.Item>
    <Menu.Item>
      <a onClick={openContactModal}>Contato</a>
    </Menu.Item>
    </Menu>);
}
  

export default Navbar;