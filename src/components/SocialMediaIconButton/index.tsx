import * as React from 'react';

import style from './style.module.scss';
import { Icon } from 'antd';

interface ISocialMediaIconButtonProps{ 
    imgType: string; 
    link: string; 
}

export const SocialMediaIconButton = ({imgType, link}: ISocialMediaIconButtonProps) => { 
    
    return (
        <a className={style.button} href={link}>
            <Icon type={imgType}/>
        </a>
    )
}