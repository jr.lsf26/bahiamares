import * as React from 'react';

import style from './style.module.scss';
import { Modal, Icon, Spin } from 'antd';
import { RoundButton } from '../RoundButton';

interface IAdditionalInfoProps { 
    page: number, 
    currentImg: string;
    onClose: () => void; 
    visible: boolean;
    forecastType: string;
    setPdfPage: (page: number) => void  ;
    loading: boolean;
}

export const AdditionalInfoModal =({ page, currentImg, onClose, visible, forecastType, setPdfPage, loading} : IAdditionalInfoProps) => { 
    return (
        
        <Modal
        className={style.modalinfo}
        title={`Informações -  ${forecastType.toLocaleLowerCase()}`}
        maskClosable={true}
        visible={visible}
        onCancel={onClose}
        onOk={onClose}
        cancelButtonProps={
            {
                hidden: true
            }
        }
        >
            <Spin spinning={loading}>
            <div className={style.modalContent}>
            <img
                className={style.helpImg}
                 src={`data:application/png;base64, ` + currentImg}
              ></img>
            
            </div>
            </Spin>
            <div className={style.buttonsDiv}>
                {page > 1 && <RoundButton onClick={() => setPdfPage(page-1)} icon={<Icon className={style.btnIcon}  type="left"/>}/>}
                {page <= 2 && <RoundButton onClick={() => setPdfPage(page+1)} icon={<Icon className={style.btnIcon} type="right"/>}/>}
            </div>
        </Modal>
    )
}
