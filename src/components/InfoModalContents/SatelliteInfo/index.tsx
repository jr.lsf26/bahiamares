import * as React from 'react';

const SatelliteInfo = () => { 
    return (
        <div>
            <p>A imagem de satélite mostra a concentração de nuvens na atmosfera através de
satélites europeus METEOSAT. A imagem de satélite é divulgada publicamente pelo
banco de dados de imagens da Divisão de satélites e Sistemas Ambientais do
INPE/CPTEC (Instituto Nacional de Pesquisas Espaciais / Centro de Previsão do Tempo
e Estudos Climáticos), sendo atualizada a cada 3 horas diariamente. Contudo, as
imagens são propriedades da Organização Europeia para a Exploração de Satélites
Meteorológicos (EUMETSAT).</p>

<p>
A série de satélites europeus METEOSAT foi responsável pelo lançamento de dez
satélites geoestacionários desde 1977. Os dados e os serviços oferecidos pela série
são principalmente voltados para meteorologia, com ênfase no apoio à previsão do
tempo, contudo os dados também podem ser utilizados em outras áreas do
conhecimento, incluindo a agricultura. Em 1987 o projeto passou a ser controlado
pela European Organization for the Exploitation of Meteorological Satellites
(EUMETSAT) e a Agência Espacial Européia (ESA) continuou a atuar no
desenvolvimento do segmento espacial.
</p>
<img src={require("../../../assets/infoIcons/CPTEC_INPE_EUMETSAT.png")}/>
<a>https://www.cptec.inpe.br/</a>
<a>https://www.eumetsat.int/</a>
        </div>
    )
}

export default SatelliteInfo;