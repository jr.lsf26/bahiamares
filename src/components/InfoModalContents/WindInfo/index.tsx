import * as React from 'react';
import s from './style.scss';

const WindInfo = () => { 
    return (
        <div className={s.container}>
            <p>A NOAA (Agência Nacional de Administração Oceânica e Atmosférica dos Estados
Unidos) é a agência provedora dos dados das previsões de ventos. A NOAA executa
diariamente o sofisticado modelo numérico* meteorológico Global Forecast System
(GFS) ®, que produz e distribui previsões meteorológicas de até 8 dias, cobrindo
diferentes regiões do globo. A bahiamares realiza operacionalmente o
processamento de dados, incluindo a produção dos gráficos e mapas.</p>
<p>O Global Forecast System (GFS) está nas operações do Serviço Climático Nacional –
NOAA desde 1980 e é continuamente aprimorado pela Divisão de Modelagem Global
de Clima e Tempo (NOAA).</p>

<img src={require("../../../assets/infoIcons/GFS.png")}/>
<p>Links úteis:</p>
<a>
https://www.noaa.gov/
</a>
<a>http://www.ncep.noaa.gov/</a>
        </div>
    )
}

export default WindInfo;