import * as React from 'react';
import s from './style.scss';
import Text from 'antd/lib/typography/Text';

const WaterQualityInfo =() => { 
    return (
        <div className={s.container}>
           <p>Conforme o Instituto do Meio Ambiente e Recursos Hídricos (INEMA):</p>
           <p>Balneabilidade é entendida como qualidade das águas destinadas à recreação de
contato primário, sendo este contato direto e prolongado onde há possibilidade de
ingerir quantidade significativa de água.</p>
<p>O monitoramento da balneabilidade no Estado da Bahia é realizado pelo Inema, através
da Coordenação de Monitoramento de Recursos Ambientais e Hídricos da Diretoria de
Fiscalização e Monitoramento Ambiental, atendendo as especificações da Resolução N.
º274/2000 do CONAMA que define critérios para classificação das águas destinadas a
recreação de contato primário.</p>
<p>A Rede Amostral de Monitoramento da balneabilidade no estado atualmente é
composta por 121 pontos, distribuídos em toda a costa baiana. As amostras de água
para analises da balneabilidade são coletadas sistematicamente semanalmente, no
período da manhã em locais com maior concentração de banhista.</p>
<p>Para o monitoramento da balneabilidade utiliza-se como microrganismo indicador de
contaminação a Escherichia coli que embora não seja, de modo geral, patogênica, sua
presença na água evidencia poluição recente de origem exclusivamente fecal humana
e/ou de animal.</p>
<p>
A condição da balneabilidade nos pontos monitorados é avaliada continuamente
durante todo o ano, analisando a densidades de bactérias em um conjunto de amostras
obtidas em cinco semanas consecutivas o que possibilita classifica-la em quatro
categorias: Excelente, Muito Boa, Satisfatória e Imprópria, podendo agrupar-se as três
primeiras na categoria Própria. Para simplificar a divulgação dos resultados passou-se a
usar a classificação como Própria ou Imprópria.
</p>
<p>
A metodologia de análise das amostras segue as recomendações do Standard Methods
for the Examination of Water and Wastewater 21th ed.
</p>

<img src={require("../../../assets/infoIcons/inema.png")}/>

<Text>Links úteis:</Text>
        <a>
http://www.inema.ba.gov.br/servicos/monitoramento/qualidade-das-praias</a>
        </div>
    )
}

export default WaterQualityInfo;