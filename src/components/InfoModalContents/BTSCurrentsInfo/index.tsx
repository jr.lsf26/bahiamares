import * as React from 'react';

import style from './style.scss';

const BTSCurrentsInfo = () => { 
    return (
        <div className={style.container}>
            <p>Baseado em 1 ano contínuo de medição de correntes por correntômetro (ADCP*) na
entrada da Baía de Todos os Santos, em um ponto que representa a hidrodinâmica
do Canal de Salvador (Figura ilustrativa abaixo), foi identificado que a oscilação da
maré explica cerca de 90 % das oscilações das correntes nesta região (restante
explicado por processos não-mareais, tais como o vento, por exemplo). Em outras
palavras, a oscilação da maré astronômica é o mecanismo principal responsável pela
geração de correntes instantâneas na BTS. A partir disso, como a maré astronômica
é previsível conforme o método de Análise Harmônica*, conforme Pawlowicz et al.
(2002), a corrente de maré também é previsível da mesma forma. A bahiamares
aplicou esta metodologia e comparou os resultados das correntes de maré observada
e das correntes de maré prevista. Os resultados da previsão mostraram-se
consistentes com a realidade, sendo o valor de correlação igual a 98 % e um
percentual de erro médio da magnitude (intensidade) da corrente na ordem de 18
%.</p>

<img className={style.firstImg} src={require("../../../assets/infoIcons/VALIDACAO_A2_V_2.png")}/>
<img className={style.secondImg} src={require("../../../assets/infoIcons/Localizacao_A2.png")}/>


<p>*Perfilador Acústico Doppler. Correntômetro fundeado sob responsabilidade do Grupo de
Oceanografia Tropical da Universidade Federal da Bahia (GOAT-UFBA). Através de ondas acústicas,
este equipamento é capaz de medir, em curtos intervalos de tempo, a velocidade e direção de
correntes para diferentes camadas da coluna d’água.</p>
<p>*A partir de uma longa série temporal contínua de dados medidos de correntes, para uma região
caracterizada por correntes dominadas por maré, pode-se aplicar o método da Análise Harmônica
(Pawlowicz et al. 2002) para gerar a previsão. Este método geofísico identifica as principais
componentes harmônicas da corrente de maré da região, incluindo suas respectivas amplitudes e
fases, permitindo a previsão da corrente de maré astronômica em grande escala de tempo.</p>
<p>*A partir de uma longa série temporal contínua de dados medidos de correntes, para uma região
caracterizada por correntes dominadas por maré, pode-se aplicar o método da Análise Harmônica
(Pawlowicz et al. 2002) para gerar a previsão. Este método geofísico identifica as principais
componentes harmônicas da corrente de maré da região, incluindo suas respectivas amplitudes e
fases, permitindo a previsão da corrente de maré astronômica em grande escala de tempo.</p>
<p>Referências:
Pawlowicz, R., Beardsley, B., Lentz, S. 2002. Classical tidal harmonic analysis including error estimates
in MATLAB using T_TIDE. Computer & Geosciences v. 28, p. 929–937, 2002.
https://www.sciencedirect.com/science/article/pii/S0098300402000134</p>

        </div>
    )
}

export default BTSCurrentsInfo;