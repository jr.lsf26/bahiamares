import * as React from 'react';

const TSMInfo = () => { 
    return ( 
        <div>
           <p>A fonte de dados da temperatura da superfície do mar é o consórcio HYCOM (Hybrid
Coordinate Ocean Model - HYCOM). HYCOM é um modelo numérico* oceânico que
representa tridimensionalmente o estado dos oceanos em alta resolução, em tempo
real e futuro, sendo fruto de um esforço multi-institucional patrocinado pelo
Programa Nacional de Parcerias Oceânicas (NOPP), como parte do Experimento
Mundial de Assimilação de Dados do Oceano (GODAE). A bahiamares realiza
operacionalmente o processamento de dados, incluindo a produção dos gráficos e
mapas.</p>

<p>
Os membros da parceria do consócio HYCOM são o Centro da Universidade Estadual
da Flórida para Estudos de Predição Atmosférica Oceânica (FSU / COAPS), a Escola de
Ciências Marinhas e Atmosféricas da Universidade de Miami Rosenstiel (UM /
RSMAS), o Laboratório de Pesquisa Naval / Stennis Space Center (NRL / STENNIS ), o
Serviço Oceanográfico Naval (NAVOCEANO), o Centro de Meteorologia e
Oceanografia da Frota (FNMOC), o Laboratório de Investigação Naval / Monterey
(NRL / MONTEREY), a Administração Nacional Oceanográfica e Atmosférica / Centros
Nacionais de Previsão Atmosférica / Modelação e Análise Marinha Sucursal (NOAA /
NCEP / MMAB), Serviço Nacional Oceânico NOAA (NOAA / NOS), Laboratório
Oceanográfico e Meteorológico Atlântico NOAA (NOAA / AOML), Laboratório
Ambiental Marinho do Pacífico NOAA (NOAA / PMEL), Planning Systems Inc.,
Laboratório Nacional Los Alamos (LANL), Serviço Hidrográfico e Océanográfico da
Marinha (SHOM), Laboratoire des Ecoulements Géophysiques et Industriels (LEGI), O
Projeto de Código Aberto para uma rede Network Protocolo de acesso a dados
(OPeNDAP), Universidade da Carolina do Norte (UNC), Universidade Rutgers,
Universidade do Sul da Flórida (USF), Fugro-GEOS / Ocean Numerics, Horizon Marine
Inc., Roffer's Ocean Fishing Forecasting Service Inc. (ROFFS ), Orbimage, Shell Oil
Company, ExxonMobil Corp., NOAA / Serviço Nacional de Meteorologia / Centro de
Previsão Tropical (NOAA / NWS / TPC), NOAA / Serviço Nacional de Meteorologia /
Centro de Previsão de Oceanos (NOAA / NWS / OPC), Universidade de Michigan, e da
Universidade das Ilhas Virgens (UVI).
</p>
<img src={require("../../../assets/infoIcons/hycom.jpg")}/>
        </div>
    )
}

export default TSMInfo;