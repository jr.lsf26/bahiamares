import * as React from 'react';

const TideInfo = () => { 
    return (
        <div >
            <p>A previsão da maré astronômica da bahiamares é resultante da aplicação do método
de Análise Harmônica* em série de dados medidos do nível d’água da região. Com
base na comparação entre 1 ano de dados medidos e previsto, em um ponto
próximo à Salvador, os resultados da previsão mostraram-se extremamente
condizentes com a realidade. O índice de correlação foi igual a 99 % e erro médio
(rmse) foi igual a 5 centímetros (figura ilustrativa abaixo). A previsão da maré
astronômica pode ser extrapolada para demais regiões litorâneas adjacentes a
Salvador, pois estão sob a mesma influência astronômica. Por exemplo, os horários
da maré toda alta ou toda baixa em Salvador e em Ilhéus são extremamente
próximos (poucos minutos de diferença), bem como as alturas da maré.</p>

<p>* A partir de uma longa série temporal contínua de dados medidos do nível d’água da região (mínimo 2
meses), pode-se aplicar o método da Análise Harmônica (Pawlowicz et al. 2002) para gerar a previsão da
maré astronômica. Este método geofísico identifica as principais componentes harmônicas da maré da
região, incluindo suas respectivas amplitudes e fases, permitindo a previsão da maré astronômica em
grande escala de tempo.</p>

<p>Referências:

Pawlowicz, R., Beardsley, B., Lentz, S. 2002. Classical tidal harmonic analysis including error estimates in
MATLAB using T_TIDE. Computer & Geosciences v. 28, p. 929–937, 2002.
https://www.sciencedirect.com/science/article/pii/S0098300402000134</p>
        </div>
    )
}

export default TideInfo;


