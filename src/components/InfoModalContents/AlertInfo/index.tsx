import * as React from 'react';


const AlertInfo = () => { 
    return ( 
       <> <p>
           O sistema operacional de alerta oceânico desenvolvido pela bahiamares tem como
base os valores dos seguintes parâmetros físicos próximo à costa: altura e período de
ondas, e velocidade dos ventos. O sistema de alerta oceânico tem como objetivo
divulgar os resultados das previsões oceânicas-atmosféricas de forma direta para os
usuários. Atualizado diariamente, o alerta oceânico abrange uma janela de tempo de
8 dias (dia atual + 7 dias a frente), em intervalos de 3 horas, que serve de previsão
das condições de segurança para atividades náuticas ao longo do litoral. É
recomendável o acompanhamento diário das atualizações dos alertas.</p>
<p>
O alerta oceânico não é apropriado para regiões confinadas (interiorizadas) tais como
rios, baías, estuários e sistemas costeiros protegidos por barreiras físicas naturais ou
antrópicas. A bahiamares não se responsabiliza por qualquer eventualidade
decorrente do uso deste sistema de alerta oceânico. O uso do alerta oceânico
fornecido pela bahiamares é de total responsabilidade do usuário. Os valores dos
parâmetros físicos (ondas e ventos) são advindos do sofisticado modelo numérico
WaveWatch III executado pela NOAA (Agência Nacional de Administração Oceânica e
Atmosférica dos Estados Unidos).
</p>
        </>
    )
}

export default AlertInfo;