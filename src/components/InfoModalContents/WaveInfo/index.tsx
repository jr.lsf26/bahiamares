import * as React from 'react';
import Text from 'antd/lib/typography/Text';
import s from './style.scss';

const WaveInfo = () => { 
    return (
        <div className={s.box} >
            <p>A NOAA (Agência Nacional de Administração Oceânica e Atmosférica dos Estados
Unidos) é a agência provedora dos dados das previsões de ondas e ventos. A NOAA
executa 4 vezes por dia o sofisticado modelo numérico* oceânico-atmosférico
WaveWatch III®, que produz e distribui previsões de ondas e ventos de até 8 dias,
cobrindo diferentes regiões do globo. A bahiamares realiza operacionalmente o
processamento de dados, incluindo a produção dos gráficos e mapas.</p>
<p>Os dados de validação (comparação entre os resultados do modelo e das medições
por boias oceanográficas) estão disponíveis de julho de 2013 até o presente em<a>https://polar.ncep.noaa.gov/waves/validation/prod/.</a> Também, a bahiamares
comparou os resultados de altura significativa de onda do modelo (WW3) com as
medições por ondógrafo localizado na Praia do Forte (5 km fora da costa), Bahia
(figura ilustrativa abaixo). Com base em 3 anos de comparação de dados
(2014-2017), os resultados do modelo numérico WaveWatch III mostraram-se muito
condizentes com a realidade, sendo o valor do índice de correlação igual a 90 % e o
erro médio (rmse) igual a 0,2 metros (20 cm).</p>

<img src={require("../../../assets/infoIcons/wavewatch.png")}/>
<img src={require("../../../assets/infoIcons/noaa_2.png")}/>
<img className={s.imgSize} src={require("../../../assets/infoIcons/ww3_x_a0_2.png")}/>


        <Text>Links úteis:</Text>
        <a>https://www.noaa.gov/</a>
        </div>
    )
}

export default WaveInfo;