import * as React from 'react';


const WeatherInfo =() => { 
    return ( 
        <div>
             <p>Na meteorologia, a carta sinótica do tempo é originada a partir de um amplo
monitoramento de parâmetros meteorológicos (ex: pressão atmosférica, ventos e
temperatura do ar). A carta sinótica (synoptikos, em grego, significa proporcionar
uma visão geral do todo) é uma espécie de “screenshot” da condição atmosférica de
um determinado local em um dado momento (horário), que apresenta informações
valiosas tais como sistemas de baixa e alta pressão atmosférica, frentes frias e
quentes, tempestades tropicais e subtropicais, jatos atmosféricos e etc. Diariamente,
a carta sinótica do tempo é divulgada publicamente pelo INPE/CPTEC (Instituto
Nacional de Pesquisas Espaciais / Centro de Previsão do Tempo e Estudos Climáticos)
para um domínio que cobre toda a América do Sul.</p>

<img src={require("../../../assets/infoIcons/CPTEC_INPE.png")}/>
<a>https://www.cptec.inpe.br/</a>
        </div>
    )
}

export default WeatherInfo;