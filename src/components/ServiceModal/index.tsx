import * as React from 'react';

import style from './style.module.scss';
import { Modal, Row, Col } from 'antd';
import { AddressBar, IAddress } from '../AddressBar';
import { ContactWithLogo, IContact } from '../ContactWithLogo';

interface IForecastInfoModal { 
    visible: boolean,
    onCancel: () => void;
    address: IAddress;
    contacts: IContact[];
    serviceType: string;
    logos: string[];
    setServiceNumber: (change: number) => void;
    serviceNumber: number;
    maxPage: number;
    serviceName: string;
}

export const ServiceModal =({ visible, onCancel, address, contacts, serviceType, logos, setServiceNumber, serviceNumber, maxPage, serviceName} : IForecastInfoModal) => { 


    return (
        <Modal
        className={style.modalinfo}
        title={`Informações sobre ${serviceType}`}
        maskClosable={true}
        visible={visible}
        onCancel={onCancel}
        width={"40%"}
        >
            <div className={style.modalContent}>
                <div className={style.upperBox}>
                <button disabled={serviceNumber === 0} onClick={() => setServiceNumber(serviceNumber - 1)}> {"< Anterior"}</button>
                <button disabled={serviceNumber === maxPage} onClick={() => setServiceNumber(serviceNumber + 1)}>Próximo ></button>
                </div>
                <div className={style.lowerBox}>
                <Row className={style.row} type={"flex"} justify={"center"} >
                    <Col span={10}>
                    <img className={style.firstLogo} src={require(`../../assets/modalService/${logos[0]}.png`)}/>
                    </Col>
                    <Col span={14}>
                    <div className={style.companyInfo}>
                        <div className={style.upperInfoBox}>
                            <h2>{serviceName}</h2>
                            <AddressBar address={address}/>
                            <div className={style.contactsBox}>
                                {contacts.map(contact => ( 
                                    <ContactWithLogo key={contact.contact} contactObj={contact}/>
                                ))}
                            </div>
                        </div>
                        <div className={style.lowerInfoBox}>
                            <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>
                            <img className={style.lowerLogo} src={require(`../../assets/images/infoImgs/${logos[1]}.jpg`)}/>
                        </div>
                    </div>
                    </Col>
                </Row>
                
                </div>
                
            </div>

        </Modal>
    )
}
