import * as React from 'react';
import {  Alert } from 'antd';

import style from './style.module.scss';

export interface IAddress{ 
    street: string;
    stNumber: string;
    neighborhood: string;
    city: string;
    uf: string;
}

interface IProps{
    address: IAddress;

}

export const AddressBar = ({address} : IProps) => { 

    const formatAddress = (addr: IAddress) => {
        return `${addr.street}, ${addr.stNumber} - ${addr.neighborhood}, ${addr.city} - ${addr.uf}`
    }
    return (
        <Alert className={style.alert} message={formatAddress(address)} showIcon iconType={"compass"} closable={false}></Alert>
    )
}