import * as React from "react";
import s from './style.module.scss';

interface IProps{
  imgSrc: string;
  handleClick: (selected: string) => void;
  serviceName?: string;
  disabled?: boolean;
}
const ServiceType = (props: IProps) => (
    <div className={s.imgBox} onClick={() => props.disabled ? null : props.handleClick(props.imgSrc)}>
        <img className={s.img} src={require('../../assets/images/' + props.imgSrc + '.png')}></img>
        <h4>{props.serviceName}</h4>
    </div>    
)

export default ServiceType;