import * as React from 'react';

import style from './style.module.scss';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { checkEmail } from '../../utils/formatters';
import TextArea from 'antd/lib/input/TextArea';


interface ISponsorFormProps extends FormComponentProps { 
    onSubmit: (values: any) => void;
    visible: boolean;
    closeModal: () => void;
}

export const InnerSponsorForm = ({onSubmit, visible, closeModal, form, ...rest}: ISponsorFormProps) => {

    const {getFieldDecorator, getFieldsValue, setFieldsValue} = form;

    const handleSubmit = (e: React.FormEvent) => { 
        setFieldsValue({type: 'Patrocinador'})
        e.preventDefault();
        onSubmit(getFieldsValue());
    }

    return(
        <Modal
        visible={visible}
        title="Patrocinador"
        okText="Enviar"
        onCancel={closeModal}
        maskClosable={true}
        onOk={handleSubmit}
        destroyOnClose={true}
        cancelText="Fechar"
        >

            <Form className={style.form} layout="vertical" {...rest}>
                <Row>
                    <Col span={24}>
                        <p>Promova sua marca e incentive o desenvolvimento da bahiamares! Entre já em contato e conheça os benefícios de ser um patrocinador e incentivador!</p>
                    </Col>
                </Row>
                <Row gutter={8}>
                    <Col span={12}>
                        <Form.Item >
                                {getFieldDecorator("name", {
                                    rules: [
                                        {message: "Nome é obrigatório!",
                                        required: true
                                    }
                                    ]
                                })
                                (
                                    <Input placeholder={"Nome"}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item >
                                {getFieldDecorator("email", {
                                    rules: [
                                        {message: "Nome é obrigatório!",
                                        required: true
                                    },{
                                        message: "Insira um e-mail em um formato válido!",
                                        validator: checkEmail
                                    }
                                    ]
                                })
                                (
                                    <Input placeholder={"E-mail"}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    <Form.Item >
                                {getFieldDecorator("phone", {
                                    rules: [
                                        {message: "Telefone é obrigatório",
                                        required: true
                                    }]
                                })
                                (
                                    <Input placeholder={"Telefone"}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    <Form.Item >
                                {getFieldDecorator("message", {
                                    rules: [
                                        {message: "Mensagem é obrigatório",
                                        required: true
                                    }]
                                })
                                (
                                    <TextArea placeholder={"Digite sua mensagem"}></TextArea>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    <Form.Item >
                                {getFieldDecorator("type", {
                                })
                                (
                                    <Input disabled className={style.hiddenInput}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Modal>
    )

 }

 export const SponsorForm = Form.create<ISponsorFormProps>({name: "sponsor_form"})(InnerSponsorForm);