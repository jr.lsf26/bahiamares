import * as React from 'react';

import style from './style.module.scss';
import { Modal, Form, Row, Col, Input, Select, Alert } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { checkEmail } from '../../utils/formatters';
import TextArea from 'antd/lib/input/TextArea';
import { SelectValue } from 'antd/lib/select';


interface IPartnerFormProps extends FormComponentProps { 
    onSubmit: (values: any) => void;
    visible: boolean;
    closeModal: () => void;
}

export const InnerPartnerForm = ({onSubmit, visible, closeModal, form, ...rest}: IPartnerFormProps) => {

    const {getFieldDecorator, getFieldsValue, setFieldsValue} = form;

    const [showForm, setShowForm] = React.useState<SelectValue>("");

    const handleSubmit = (e: React.FormEvent) => { 
        setFieldsValue({type: 'Parceiro'})
        e.preventDefault();
        onSubmit(getFieldsValue());
    }

    const changeSelect = (value: SelectValue) => { 
        setShowForm(value)
    }

    const closeAndResetModal = () => { 
        setShowForm("")
        closeModal();
    }

    return(
        <Modal
        visible={visible}
        title="Parceiros"
        okText="Enviar"
        onCancel={closeAndResetModal}
        maskClosable={true}
        onOk={handleSubmit}
        destroyOnClose={true}
        cancelText="Fechar"
        >

            <Form className={style.form} layout="vertical" onSubmit={handleSubmit} {...rest}>
                {showForm === "sim" && (
                    <Alert closable={false} type="success" message="Promova seu serviço e incentive o desenvolvimento da bahiamares! Entre já em contato e conheça os benefícios de ser um parceiro e incentivador!"></Alert>
                )}
                {
                    showForm === "outros" && (
                        <Alert closable={false} type="warning" message="Entre já em contato e conheça os benefícios de ser um parceiro e incentivador!"></Alert>
                    )
                }
                <Row>
                    <Col span={24}>
                        <Form.Item label="Quer promover seu serviço naútico?">
                        <Select value={showForm} defaultValue={""} onChange={changeSelect} placeholder="-- Selecione --">
                            <Select.Option value={""}>-- Selecione --</Select.Option>
                            <Select.Option value={"sim"}>Sim</Select.Option>
                            <Select.Option value={"outros"}>Outros</Select.Option>
                        </Select>
                        </Form.Item>
                    </Col>
                </Row>
                {showForm !== "" && ( 
                    <>
                    <Row gutter={8}>
                    <Col span={24}>
                        <Form.Item >
                                {getFieldDecorator("name", {
                                    rules: [
                                        {message: "Nome é obrigatório!",
                                        required: true
                                    }
                                    ]
                                })
                                (
                                    <Input placeholder={"Nome"}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                    </Row>
                    <Row>
                    <Col span={24}>
                        <Form.Item >
                                {getFieldDecorator("email", {
                                    rules: [
                                        {message: "Nome é obrigatório!",
                                        required: true
                                    },{
                                        message: "Insira um e-mail em um formato válido!",
                                        validator: checkEmail
                                    }
                                    ]
                                })
                                (
                                    <Input placeholder={"E-mail"}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    <Form.Item >
                                {getFieldDecorator("phone", {
                                })
                                (
                                    <Input placeholder={"Telefone"}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    <Form.Item >
                                {getFieldDecorator("message", {
                                })
                                (
                                    <TextArea placeholder={"Digite sua mensagem"}></TextArea>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    <Form.Item >
                                {getFieldDecorator("type", {
                                })
                                (
                                    <Input disabled className={style.hiddenInput}></Input>
                                )
                                }
                        </Form.Item>
                    </Col>
                </Row>
                </>
                )}
                
            </Form>
        </Modal>
    )

 }

 export const PartnerForm = Form.create<IPartnerFormProps>({name: "partner_form"})(InnerPartnerForm);