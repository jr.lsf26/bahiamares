import * as React from 'react';

import style from './style.module.scss';
import { IInfoContent } from '../../utils/forecastInfoBarContent';
import { Modal } from 'antd';

interface IForecastInfoModal { 
    infoContent: IInfoContent,
    visible: boolean,
    onCancel: () => void;
}

export const ForecastInfoModal =({infoContent, visible, onCancel} : IForecastInfoModal) => { 


    return (
        <Modal
        className={style.modalinfo}
        title={`Informações sobre ${infoContent.title}`}
        maskClosable={true}
        okButtonProps={{
            hidden: true
        }}
        cancelButtonProps={{
            hidden: true
        }}
        visible={visible}
        onCancel={onCancel}
        >

            <div className={style.modalContent}>
                <img src={require(`../../assets/images/infoImgs/${infoContent.imgSrc}.jpg`)}></img>
                <div>
                {infoContent.info.map(info => (
                    <p>{info}</p>
                ))}
                </div>
            </div>

        </Modal>
    )
}
