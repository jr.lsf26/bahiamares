const addZero =(i: number) => {
    let j;
    if(i < 10){
      j = '0' + i;
      return j;
    }
    else{
      j = i;
      return j;
    }
  }

  const FormatHours = (date: Date) => { 
    let h = addZero(date.getHours());
    let m = addZero(date.getMinutes());
    let x = h.toString() + m.toString();
    return x;
  }

  export default FormatHours;