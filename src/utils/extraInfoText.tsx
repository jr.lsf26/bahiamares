import AlertInfo from '../components/InfoModalContents/AlertInfo';
import * as React from 'react';
import WaveInfo from '../components/InfoModalContents/WaveInfo';
import WindInfo from '../components/InfoModalContents/WindInfo';
import WaterQualityInfo from '../components/InfoModalContents/WaterQualityInfo';
import WeatherInfo from '../components/InfoModalContents/WeatherInfo';
import SatelliteInfo from '../components/InfoModalContents/SatelliteInfo';
import TSMInfo from '../components/InfoModalContents/TSMInfo';
import CurrentsInfo from '../components/InfoModalContents/CurrentsInfo';
import TideInfo from '../components/InfoModalContents/TideInfo';
import BTSCurrentsInfo from '../components/InfoModalContents/BTSCurrentsInfo';
import { GenericContent } from '../components/GenericContent';

export const setInfoText = (forecastType: string, displayType: string, region: string) => { 
    if(forecastType === "ALERTA"){
        return <AlertInfo></AlertInfo>
    }
    if(forecastType === "ONDAS"){
        return <WaveInfo></WaveInfo>
    }
    if(forecastType === "VENTO"){
        return <WindInfo></WindInfo>
    }
    if(forecastType === "QUALIDADE"){
        return <WaterQualityInfo></WaterQualityInfo>
    }
    if(forecastType === "TEMPO" && displayType === "MAPA"){
        return <WeatherInfo></WeatherInfo>
    }
    if(forecastType === "TEMPO" && displayType === "SATELITE"){
        return <SatelliteInfo></SatelliteInfo>
    }
    if(forecastType === "TSM"){ 
        return <TSMInfo></TSMInfo>
    }if(forecastType === "CORRENTES" && region === "BTS"){
        return <BTSCurrentsInfo></BTSCurrentsInfo>
    }
    if(forecastType === "CORRENTES"){
        return <CurrentsInfo></CurrentsInfo>
    }
    
    if(forecastType === "MARE"){
        return <TideInfo></TideInfo>
    }
    return <GenericContent></GenericContent>
}