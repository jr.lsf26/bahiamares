import * as React from 'react';
import { AlertMapContents } from '../components/HowToReadContents/AlertContents';
import { WaveMapContents } from '../components/HowToReadContents/WaveContents';
import { WindContents } from '../components/HowToReadContents/WindContents';
import { TideContents } from '../components/HowToReadContents/TideContents';
import { GenericContent } from '../components/GenericContent';
import { CurrentsMapContents, CurrentsGrafContents } from '../components/HowToReadContents/CurrentsContents';
import { SinoticaContents } from '../components/HowToReadContents/WeatherContents';
import { QualityContents } from '../components/HowToReadContents/QualityContents';
import { TSMContents } from '../components/HowToReadContents/TSMContents';

export const howToReadText = (forecastType: string, displayType: string) => { 
    if(forecastType === "ALERTA" && displayType === "MAPA"){
        return <AlertMapContents></AlertMapContents>
    }
    if(forecastType === "ONDAS" && displayType === "MAPA"){
        return <WaveMapContents></WaveMapContents>
    }
    if(forecastType === "VENTO" && displayType === "MAPA"){
        return <WindContents></WindContents>
    }
    if(forecastType === "MARE" && displayType === "GRAF"){
        return <TideContents></TideContents>
    }
    if(forecastType === "CORRENTES" && displayType === "MAPA"){
        return <CurrentsMapContents></CurrentsMapContents>
    }
    if(forecastType === "CORRENTES" && displayType === "GRAF"){
        return <CurrentsGrafContents></CurrentsGrafContents>
    }
    if(forecastType === "TEMPO" && displayType === "MAPA"){
        return <SinoticaContents></SinoticaContents>
    }
    if(forecastType === "QUALIDADE"){
        return <QualityContents/>
    }
    if(forecastType === "TSM"){ 
        return <TSMContents></TSMContents>
    }
    
    return <GenericContent></GenericContent>
}