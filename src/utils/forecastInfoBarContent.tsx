
export interface IInfoContent{
    title: string;
    info: string[];
    imgSrc: string;
}

export const forecastInfoContent: {[key: string]: IInfoContent} = {

        ALERTA: {
            title: "Alerta Oceânico",
            info: [
                "O Sistema Operacional de Alerta Oceânico desenvolvido pela bahiamares tem como objetivo divulgar os resultados das previsões oceânicas-atmosféricas de forma popular para os usuários.",
                "Este sistema foi construído baseado em dados históricos de ondas e ventos no litoral da Bahia. Atualizado diariamente, o Alerta Oceânico abrange uma janela de tempo de 8 dias (dia atual + 7 dias a frente) que serve de indicador de segurança para atividades náuticas e recreativas ao longo do litoral baiano.",
                "A intensidade de ondas e ventos impacta na segurança em diversas atividades náuticas e recreativas no litoral da Bahia, além de potenciais danos em estruturas civis. Por isso, fique ligado!",
                "Opções visuais em gráfico e mapa!"
            ],
            imgSrc: "Alerta"
        },
        ONDAS: {
            title: "Ondas",
            info: [
                "Previsão de altura, período e direção de ondas para janela de tempo de 8 dias (dia atual + 7 dias a frente). Confira também tudo sobre a ciência das ondas no litoral da Bahia com base nos dados históricos da região.",
                "A intensidade de ondas impacta na segurança em diversas atividades náuticas e recreativas no litoral da Bahia, além de potenciais danos em estruturas civis. Por isso, fique ligado!",
                "Opções visuais em gráfico e mapa!"
            ],
            imgSrc: "Ondas"
        },
        VENTOS: {
            title: "Ventos",
            info: [
                "Previsão de velocidade e direção dos ventos para janela de tempo de 8 dias (dia atual + 7 dias a frente). Confira também tudo sobre a ciência dos ventos no litoral da Bahia com base nos dados históricos da região.",
                "A intensidade dos ventos impacta na segurança em diversas atividades náuticas e recreativas no litoral da Bahia, além de potenciais danos em estruturas civis. Por isso, fique ligado!",
                "Opções visuais em gráfico e mapa!"
            ],
            imgSrc: "Ventos"
        },
        MARE: {
            title: "Maré/Calendário Lunar",
            info: [
                "Previsão da altura e horário da maré astronômica para janela de tempo de 3 anos (ano atual + 2 anos a frente). Confira também tudo sobre a ciência da maré no litoral da Bahia.",
                "O calendário lunar mostra para cada mês as datas das 4 fases da lua, em um intervalo de tempo de 3 anos (ano atual + 2 anos a frente)!",
                "A maré impacta no nível da água do mar da sua região. Preveja quando será a melhor opção da altura do nível da água do mar para a prática de sua atividade náutica ou recreativa no litoral da Bahia.",
                "Opção visual em gráfico!"
            ],
            imgSrc: "Mare"
        },
        CORRENTES: {
            title: "Correntes no Litoral",
            info: [
                "Previsão de correntes oceânicas superficiais no litoral da Bahia para janela de tempo de 6 dias (dia atual + 5 dias a frente)! Confira também tudo sobre a ciência das correntes no litoral da Bahia com base em trabalhos científicos.",
                "As correntes oceânicas no litoral pode favorecer ou desfavorecer sua navegação pelo litoral baiano, além de promover transportes de substâncias tais como o óleo.",
                "Opção visual em gráfico!"
            ],
            imgSrc: "Correntes"
        },
        CORRENTESBTS: {
            title: "Correntes na BTS",
            info: [
                "Previsão de Correntes de Maré na Baía de Todos os Santos para janela de tempo de 1 ano! Confira e aprenda também tudo sobre a ciência das Correntes de Maré na BTS.",
                "As Correntes de Maré na BTS potencialmente podem impactar a segurança de praticantes de atividades náuticas esportivas e recreativas nas águas da baía, além de beneficiar ou prejudicar o desempenho de sua atividade na água. Por isso, fique ligado!",
                "Opções visuais em gráfico ou mapa!"
            ],
            imgSrc: "Correntes_BTS"
        },
        TEMPO: {
            title: "Tempo",
            info: [
               "Previsão de chuvas, temperatura do ar e horário do nascer/pôr-do-sol para um intervalo de tempo de 7 dias. Confira e aprenda também tudo sobre o clima do litoral da Bahia com base em dados históricos da região."
            ],
            imgSrc: "Tempo"
        },
        SATELITE: {
            title: "Imagem de Satélite",
            info: [
                "A imagem de satélite mostra a concentração e a propagação de nuvens na atmosfera através de satélites europeus METEOSAT (Organização Europeia para a Exploração de Satélites Meteorológicos). As imagens são disponibilizadas pelo INPE/CPTEC (Centrode Previsão do Tempo e Estudos Climáticos, que faz parte do Instituto Nacional de Pesquisas Espaciais do Brasil)",    
                "Opção visual em mapa!"
            ],
            imgSrc: "Satelite"
        },
        SINOTICA: {
            title: "Carta Sinótica do Tempo",
            info: [
                "A Carta Sinótica do Tempo é uma espécie de “screenshot” das condições atmosférica da região e é capaz de mostrar as localizações de frentes frias e quentes, sistemas de baixa e alta pressão, e outros. A Carta Sinótica é oferecida pelo INPE-CPTEC (Centro de Previsão do Tempo e Estudos Climáticos, que faz parte do Instituto Nacional de Pesquisas Espaciais do Brasil)."
            ],
            imgSrc: "Carta_Sinotica"
        },
        QUALIDADE: {
            title: "Qualidade da Água",
            info: [
                "Boletim de qualidade da água emitido semanalmente às sextas-feiras pelo INEMA (Instituto de Meio Ambiente e Recursos Hídricos da Bahia). O monitoramento da balneabilidade no Estado da Bahia é realizado pelo INEMA, através da Coordenação de Monitoramento de Recursos Ambientais e Hídricos da Diretoria de Fiscalização e Monitoramento Ambiental, atendendo as especificações da Resolução N.o274/2000 do CONAMA que define critérios para classificação das águas destinadas a recreação de contato primário."
            ],
            imgSrc: "Qualidade_Agua"
        },
        TEMPERATURA: {
            title: "Temperatura da Água",
            info: [
                "Previsão da temperatura da superfície do mar para janela de tempo de 6 dias (dia atual + 5 dias a frente)!",
               "Opção visual em mapa!"
            ],
            imgSrc: "Temp_Agua"
        },
        SOS: {
            title: "SOS",
            info: [
                'SOS é uma sessão especial que fornece informações essenciais para a prevenção e remediação de acidentes em atividades esportivas e recreativas no litoral da Bahia. SOS foi desenvolvida com base na parceria da bahiamares com especialista em salvamento aquático'
            ],
            imgSrc: "SOS"
        },
        
}