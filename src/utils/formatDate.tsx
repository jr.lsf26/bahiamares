const FormatDate = (date: Date) => {
    
    if (date.getDate() < 10 && date.getMonth() < 9) {
        const formattedDate =
          date.getFullYear() +
          "_" +
          "0" +
          (date.getMonth() + 1) +
          "_" +
          "0" +
          date.getDate();
          return formattedDate;
      } else if (date.getDate() < 10) {
        const formattedDate =
          date.getFullYear() +
          "_" +
          (date.getMonth() + 1) +
          "_" +
          "0" +
          date.getDate();
          return formattedDate;

      } else if (date.getMonth() < 9) {
        const formattedDate =
          date.getFullYear() +
          "_" +
          "0" +
          (date.getMonth() + 1) +
          "_" + date.getDate();
          return formattedDate;

      } else {
        const formattedDate =
          date.getFullYear() +
          "_" +
          (date.getMonth() + 1) +
          "_" +
          date.getDate();
          return formattedDate;

    }
}

export default FormatDate;