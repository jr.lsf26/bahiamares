import { IAddress } from '../components/AddressBar';
import { IContact } from '../components/ContactWithLogo';

export interface IService { 
    name: string;
    logos: string[];
    address: IAddress;
    contacts: IContact[];
}

export const servicesMocked: {[key: string]: IService[]} = {
    surf: [{
        name: "HCT – Sports House",
        logos: [
            "1_HCT", "2_HCT"
        ],
        address: {
            street: "Rua Des. Manoel de Andrade Teixeira", 
            stNumber: "272", 
            city: "Salvador",
            neighborhood: "Praia do Flamengo", 
            uf: "BA"
        },
        contacts: [
            {
                contact: "(71) 2137-2885",
                logo: "telephone"
            },
            {
                contact: "@hct_sports_",
                logo: "instagram"
            }
            ,{
                contact: "mkt@hctsurf.com.br",
                logo: "email"
            }

        ]
    }]
}