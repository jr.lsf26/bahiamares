export const checkShoreRegion = (region: string) => {
    if (region === "BTS"){
      return "LN";
    }
    else{
      return "LC";
    }
  }

export const sunsetRegion: {[key: string]: string} = {
    BTS: "45845",
    COND: "45847",
    SAUI: "45842",
    PF: "45842",
    AREM: "45474",
    LAUR: "56998",
    SSA: "45447",
    VAL: "62034",
    MSP: "45990",
    BOIP: "45990",
    CAMA: "45769",
    ITAC: "45889",
    ILHE: "45575",
    CANA: "45815",
    PS: "45603",
    TRANC: "45823",
    CARAV: "45892",
    MUC: "62058",
    CABRA: "45827",
    PRAD: "45695",
    ALCO: "45844",
    NV: "56754",
    MUCU: "62058"
  }

  export const regionsName: {[key: string]: string} = {
    BTS: "BAÍA DE TODOS OS SANTOS",
    COND: "CONDE",
    SAUI: "SAUIPE",
    PF: "PRAIA DO FORTE",
    AREM: "AREMBEPE",
    LAUR: "LAURO DE FREITAS",
    SSA: "SALVADOR",
    VAL: "VALENÇA",
    MSP: "MORRO DE SÃO PAULO",
    BOIP: "BOIPEBA",
    CAMA: "CAMAMU",
    ITAC: "ITACARÉ",
    ILHE: "ILHÉUS",
    CANA: "CANAVIEIRAS",
    PS: "PORTO SEGURO",
    TRANC: "TRANCOSO",
    CARAV: "CARAVELAS",
    MUC: "MUCUGÊ",
    CABRA: "SANTA CRUZ DE CABRÁLIA",
    PRAD: "PRADO",
    ALCO: "ALCOBAÇA",
    NV: "NOVA VIÇOSA",
    MUCU: "MUCURI"
  }

  export const weatherRegion: {[key: string]: string} = { 
    SSA: "n12d97n38d50/salvador/",
    BTS: "n12d26n40d22/todos-os-santos/",
    COND: "n11d81n37d61/conde/",
    SAUI: "n12d36n37d92/sauipe-de-dentro/",
    PF: "n12d57n38d00/praia-do-forte/",
    AREM: "n12d77n38d19/arembepe/",
    LAUR: "n12d88n38d31/lauro-de-freitas/",
    VAL: "n13d37n39d07/valenca/",
    MSP: "n13d38n38d91/morro-de-sao-paulo/",
    BOIP: "n13d49n39d05/cairu/",
    CAMA: "n13d94n39d11/camamu/",
    ITAC: "n14d28n38d99/itacare/",
    ILHE: "n14d79n39d05/ilheus/",
    CANA: "n15d68n38d95/canavieiras/",
    PS: "n16d44n39d07/porto-seguro/",
    TRANC: "n16d60n39d11/trancoso/",
    CARAV: "n17d73n39d27/caravelas/",
    MUC: "n13d01n41d37/mucuge/",
    CABRA: "n16d28n39d02/santa-cruz-cabralia/",
    PRAD: "n17d33n39d23/prado/",
    ALCO: "n17d73n39d27/caravelas/",
    NV: "n17d73n39d27/caravelas/",
    MUCU: "n17d73n39d27/caravelas/"
  }