
require("dotenv").config({
	path: `.env.production`,
});

module.exports = {
	siteMetadata: {
		title: `Gatsby Default Starter`,
		description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
		author: `@gatsbyjs`,
	},
	plugins: [ {
		resolve: `gatsby-plugin-create-client-paths`,
		options: { prefixes: [`/*`] },
	  },
		`gatsby-plugin-react-helmet`,
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: `${__dirname}/src/assets/images`,
			},
		},
		{
			resolve: `gatsby-plugin-google-analytics`,
			options: {
			  trackingId: process.env.GATSBY_GOOGLE_ANALYTICS_TRACKING_ID || 'none' ,
			},
		  },
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `gatsby-starter-default`,
				short_name: `starter`,
				start_url: `/`,
				background_color: `#297ecd`,
				theme_color: `#297ecd`,
				display: `minimal-ui`,
				icon: `src/assets/icons/favicon.png`, // This path is relative to the root of the site.
			},
		},
		`gatsby-plugin-typescript`,
		`gatsby-plugin-typescript-checker`,
		{
			resolve: `gatsby-plugin-sass`,
			options: {
				implementation: require("sass"),
				importLoaders: 1,
				localIdentName: "[local]_[name]",
				includePaths: ["./src/assets/styles/"],
			},
			
		},
		// this (optional) plugin enables Progressive Web App + Offline functionality
		// To learn more, visit: https://gatsby.dev/offline
		// `gatsby-plugin-offline`,
	],
	
};
